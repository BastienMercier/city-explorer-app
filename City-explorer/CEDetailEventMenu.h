//
//  CEDetailEventMenu.h
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEDetailMenuDelegate.h"

@interface CEDetailEventMenu : UIView

@property (strong, nonatomic) UIView	*descriptionView;
@property (strong, nonatomic) UIView	*messageView;
@property (strong, nonatomic) UILabel	*descriptionLabel;
@property (strong, nonatomic) UILabel	*messageLabel;

@property (weak, nonatomic) id<CEDetailMenuDelegate> delegate;

@end
