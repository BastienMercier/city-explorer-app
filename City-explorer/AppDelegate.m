//
//  AppDelegate.m
//  City-explorer
//
//  Created by theo damaville on 31/10/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "AppDelegate.h"
#import "SignupViewController.h"
#import "CECurrentUser.h"
#import "CEUserRequest.h"
#import "CEApiManager.h"
#import <TMCache.h>
#import "CECacheManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
	_cacheManager = [CECacheManager new];
	[_cacheManager loadDiskDataIntoMemory];
	
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]; 
    SignupViewController *controller = [SignupViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:controller];
    self.navigationController = navigationController;
    [self.window setRootViewController:navigationController];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    self.navigationController.navigationBarHidden = YES;
	
	[[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor colorWithWhite:1. alpha:1]];
	
	[self loadCurrentUser];
	
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	[_cacheManager saveCacheDataToDisk];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - toolbox

- (void)loadCurrentUser {
	
	[[CEUserRequest getAllUsersWithBlock:^(NSData *data, NSError *error) {
		if (error) {
			[CEUtils alert:@"req_error" withTitle:@"api_err"];
		} else {
			id json = JSON(data);
			NSLog(@"%@",json);
			id me = json[1];
			CEUser *user = [CEUser userWithDictionary:me];
			[CECurrentUser sharedUser].currentUser = user;
			[CECurrentUser sharedUser].token = me[@"token"];
			
			
			NSLog(@"%@",[CECurrentUser sharedUser].currentUser);
			[[TMMemoryCache sharedCache] setObject:[CECurrentUser sharedUser] forKey:@"current_user"];
        	[CEUserRequest imageForUser:[CECurrentUser sharedUser].currentUser.picturePath withBlock:^(UIImage *userImage) {
        		[CECurrentUser sharedUser].currentUser.picture = userImage;
        	}];
		}
	}] resume];
	
	[[CEUserRequest postToggleFollowUserWithId:@"547c8c56fd71da4722f4e88f" withBlock:^(NSData *data, NSError *error) {
		if (error) {
			[CEUtils alert:@"req_error" withTitle:@"api_err"];
		} else {
			id json = JSON(data);
			NSLog(@"%@",json);
		}
	}] resume];
}

@end
