//
//  CEFeedDataSource.m
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEFeedDataSource.h"
#import "CEFeedCell.h"

@implementation CEFeedDataSource

- (instancetype)initWithDelegate:(id<CEUpdatedDataSource>) delegate;
{
    self = [super init];
    if (self) {
        [self setDelegate:delegate];
    }
    return self;
}

- (void)setDelegate:(id<CEUpdatedDataSource>)delegate {
    _delegate = delegate;
    [self noticeDelegateWillUpdate];
}

#pragma mark - dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CEFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_feed"];
    if (!cell) {
        cell = [[CEFeedCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell_feed"];
    }
    return cell;
}

#pragma mark - refreshing data

- (void)reloadDataSource {
    [self noticeDelegateDidUpdate];
}

#pragma mark - delegate
- (void)noticeDelegateWillUpdate {
    if ([_delegate respondsToSelector:@selector(willUpdateDataSource)]) {
        [_delegate willUpdateDataSource];
    }
}

- (void)noticeDelegateDidUpdate {
    if ([_delegate respondsToSelector:@selector(didUpdateDataSource)]) {
        [_delegate didUpdateDataSource];
    }
}
@end
