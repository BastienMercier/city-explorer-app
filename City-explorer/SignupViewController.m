//
//  SignupViewController.m
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "SignupViewController.h"
#import "CETabBarController.h"

@implementation SignupViewController

- (void)viewDidLoad {
    _content.scrollerText.delegate = self;
    [_content.facebook addTarget:self action:@selector(handleSignup:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)loadView {
    
    _content = [[CEScrollerTuto alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view = _content;
}

#pragma mark - user event
- (void)handleSignup:(UIButton *)facebook {
    UITabBarController *tab = [CETabBarController createMeTabBarController];
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController pushViewController:tab animated:YES];
}

#pragma mark - scroll delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // img len = 134pt
    _content.scrollerImage.contentOffset = CGPointMake(_content.scrollerText.contentOffset.x*134.0/320.0, _content.scrollerImage.contentOffset.y);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;      // called when scroll view grinds to a halt
{
    int curr = scrollView.contentOffset.x / self.view.frame.size.width;
    _content.pageControl.currentPage = curr;
}
@end
