//
//  CETchatController.h
//  City-explorer
//
//  Created by Administrateur on 21/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEInvalidateDelegate.h"
#import <UIKit/UIKit.h>

@interface CETchatController : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray			*messages;
@property (strong, nonatomic) NSMutableArray	*sizes;
@property (weak, nonatomic) UITableView	*table;

@end
