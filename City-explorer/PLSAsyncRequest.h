//
//  PLSAsyncRequest.h
//  AsyncConnexion
//
//  Created by Administrateur on 10/10/13.
//  Copyright (c) 2013 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLSApi.h"


@interface PLSAsyncRequest : NSObject <NSURLConnectionDelegate>
/**
 * init a new request with blocks
 * @param block
 *	the block you need for the NSData callback
 * @return a new delegate for the request
 */
+ (PLSAsyncRequest*) requestWithBlock:(api_request_handler_t)block;

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end
