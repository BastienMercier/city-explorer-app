//
//  CETabBarController.m
//  City-explorer
//
//  Created by theo damaville on 04/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CETabBarController.h"
#import "CEFeedViewController.h"
#import "CECreateEventViewController.h"
#import "CEMyEventsViewController.h"

@implementation CETabBarController

+ (UITabBarController *) createMeTabBarController;
{

    //create a UITabBarController object
    UITabBarController *tabBarController=[[UITabBarController alloc]init];
	
	//fontColor
	//tabBarController.tabBar.tintColor = [UIColor redColor];
	//[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor redColor], UITextAttributeTextColor, nil] forState:UIControlStateSelected];
	
	// set color of unselected text to white
	[[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
														 NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]
														}
											 forState:UIControlStateNormal];
	
	// set selected and unselected icons
	UITabBarItem *item0 = [tabBarController.tabBar.items objectAtIndex:0];
	
	// this way, the icon gets rendered as it is (thus, it needs to be green in this example)
	item0.image = [[UIImage imageNamed:@"tab_plus"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	
	// this icon is used for selected tab and it will get tinted as defined in self.tabBar.tintColor
	item0.selectedImage = [UIImage imageNamed:@"tab_plus"];
	
	
    // create our view controllers
	
    CEFeedViewController *feed = [CEFeedViewController new];
    CECreateEventViewController *createEvent = [CECreateEventViewController new];
    CEMyEventsViewController *myEvents = [CEMyEventsViewController new];
	
	UIImage *plusImage = [UIImage imageNamed:@"tab_plus"];
	UIImage *plusImageSel = [UIImage imageNamed:@"tab_plus"];
	plusImage = [plusImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	plusImageSel = [plusImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	
    
	UIImage *horloge = [UIImage imageNamed:@"tab_horloge"];
	UIImage *horlogeSel = [UIImage imageNamed:@"tab_horloge"];
	horloge = [horloge imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	horlogeSel = [horlogeSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *home = [UIImage imageNamed:@"tab_home"];
    UIImage *homeSel = [UIImage imageNamed:@"tab_home"];
    home = [home imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    homeSel = [homeSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
	UITabBarItem *item1 = [[UITabBarItem alloc] initWithTitle:@"Créer soirée" image:plusImage selectedImage:plusImageSel];
	createEvent.tabBarItem = item1;
	UINavigationController *nav1 = [[UINavigationController alloc] initWithRootViewController:createEvent];

	UITabBarItem *item2 = [[UITabBarItem alloc] initWithTitle:@"Actualités" image:home selectedImage:homeSel];
	feed.tabBarItem = item2;
	UINavigationController *nav2 = [[UINavigationController alloc] initWithRootViewController:feed];

	UITabBarItem *item3 = [[UITabBarItem alloc] initWithTitle:@"Mes soirées" image:horloge selectedImage:horlogeSel];
	myEvents.tabBarItem = item3;
	UINavigationController *nav3 = [[UINavigationController alloc] initWithRootViewController:myEvents];

    tabBarController.viewControllers= [NSArray arrayWithObjects:nav1, nav2,nav3, nil];
	tabBarController.tabBar.alpha = 0.6;
	[tabBarController.tabBar setTintColor:[UIColor whiteColor]];
    return tabBarController;
}
@end
