//
//  CEApiManager.h
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CEApiManager : NSObject

@property (strong, nonatomic) NSArray *requests;
//@property (strong, nonatomic) NSDictionary *results;

- (void) executeParallel:(void (^) (void) )block;
- (void) executeSeries;

@end
