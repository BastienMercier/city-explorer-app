//
//  CEMyEventDataSource.m
//  City-explorer
//
//  Created by theo damaville on 16/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEMyEventDataSource.h"
#import "CEFeedCell.h"

@implementation CEMyEventDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CEFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_myEvent"];
    if (!cell) {
        cell = [[CEFeedCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell_myEvent"];
    }
    return cell;
}

- (void)willUpdate {
    if ([_delegate respondsToSelector:@selector(willUpdateDataSource)]) {
        [_delegate willUpdateDataSource];
    }
}

- (void)didUpdate {
    if ([_delegate respondsToSelector:@selector(didUpdateDataSource)]) {
        [_delegate didUpdateDataSource];
    }
}

@end
