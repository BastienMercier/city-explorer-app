//
//  CECacheManager.m
//  City-explorer
//
//  Created by Administrateur on 03/12/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CECacheManager.h"
#import <TMCache.h>

@implementation CECacheManager

NSString *const kCECurrentUser = @"current_user";

- (void)saveCacheDataToDisk {
	NSArray *memids = @[kCECurrentUser];
	for (NSString *ids in memids) {
		[[TMDiskCache sharedCache] setObject:[[TMMemoryCache sharedCache] objectForKey:ids] forKey:ids];
	}
}

- (void)loadDiskDataIntoMemory {
	NSArray *memids = @[kCECurrentUser];
	for (NSString *ids in memids) {
		[[TMMemoryCache sharedCache] setObject:[[TMDiskCache sharedCache] objectForKey:ids] forKey:ids];
	}
}

- (void)setObject:(id)object forKey:(NSString *)aKey {
	[[TMMemoryCache sharedCache] setObject:object forKey:aKey];
}

- (id)objectForKey:(NSString *)aKey {
	return [[TMMemoryCache sharedCache] objectForKey:aKey];
}

@end
