//
//  CEProfilViewController.m
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEProfilViewController.h"

@interface CEProfilViewController ()

@end

@implementation CEProfilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"City-Explorer";
    [self backArrow];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"profil_navbar_texture"]]];
	_profilDataSource = [CEProfileDataSource new];
	_profilDataSource.delegate = self;
}

- (void)loadView {
    _contentView = [CEProfilView new];
    self.view = _contentView;
}

- (void)backArrow {
    UIImageView *back = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back_arrow"]];
    back.frame = CGRectMake(0, 0, 15, 20);
    back.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBack)];
    [back addGestureRecognizer:tap];
    back.tag = 0;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (void)handleBack {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - profil updated 

- (void)didUpdateDataSource {
	[_contentView.topView setData:_profilDataSource.currentUser];
}

@end
