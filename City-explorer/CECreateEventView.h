//
//  CECreateEventView.h
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CETextFieldWithIcon.h"
#import "CEFormView.h"

@interface CECreateEventView : UIView

// form1
@property (strong, nonatomic) CEFormView *infosCity;
@property (strong, nonatomic) CETextFieldWithIcon *city;
@property (strong, nonatomic) CETextFieldWithIcon *date;

// form2
@property (strong, nonatomic) CEFormView    *infosOther;
@property (strong, nonatomic) UIImageView   *ageIcon;
@property (strong, nonatomic) UILabel       *ageMoyen;
@property (strong, nonatomic) UILabel       *ageDisplay;
@property (strong, nonatomic) UISlider      *ageSlider;

@property (strong, nonatomic) UILabel       *sexLabel;
@property (strong, nonatomic) UIImageView   *sexMale;
@property (strong, nonatomic) UIImageView   *sexFemale;
@property (strong, nonatomic) UIImageView   *sexBoth;

@property (strong, nonatomic) UIView        *descView;
@property (strong, nonatomic) UIImageView   *descIcon;
@property (strong, nonatomic) UIImageView   *descArrow;
@property (strong, nonatomic) UILabel       *descEvent;

@property (strong, nonatomic) UIButton      *validate;

@property (strong, nonatomic) UIView        *indicator;
@end
