//
//  CEBotDetailEventView.h
//  City-explorer
//
//  Created by theo damaville on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CETextFieldWithIcon.h"

@interface CEBotDetailEventView : UIView

@property (strong, nonatomic) CETextFieldWithIcon *date;
@property (strong, nonatomic) CETextFieldWithIcon *hour;
@property (strong, nonatomic) CETextFieldWithIcon *city;
@property (strong, nonatomic) CETextFieldWithIcon *location;
@property (strong, nonatomic) CETextFieldWithIcon *age;
@property (strong, nonatomic) UIButton            *subscribe;

@end
