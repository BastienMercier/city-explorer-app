//
//  CETopEventDetailView.h
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CETopEventDetailView : UIView

@property (strong, nonatomic) UIImageView	*userPicture;
@property (strong, nonatomic) UILabel		*userName;
@property (strong, nonatomic) UILabel		*eventName;

@end
