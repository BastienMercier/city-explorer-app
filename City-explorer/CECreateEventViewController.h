//
//  CECreateEventViewController.h
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CECreateEventView.h"

@interface CECreateEventViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) CECreateEventView *contentView;

@end
