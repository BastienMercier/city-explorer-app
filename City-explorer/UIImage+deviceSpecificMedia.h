//
//  UIImage_deviceSpecificMedia.h
//  City-explorer
//
//  Created by Administrateur on 08/12/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (deviceSpecificMedia)

+(instancetype )imageForDeviceWithName:(NSString *)fileName;

@end