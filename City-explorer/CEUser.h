//
//  CEUser.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CEUtils.h"

typedef enum kCEProvider{
	kCEProviderFacebook = 0
}kCEProvider;

@interface CEUser : NSObject

+ (CEUser *)userWithDictionary:(NSDictionary *)userInfos;

@property (nonatomic) kCEProvider      provider;
@property (strong, nonatomic) NSString *lastname;
@property (strong, nonatomic) NSString *firstname;
@property (strong, nonatomic) NSString *picturePath;
@property (strong, nonatomic) UIImage  *picture;
@property (strong, nonatomic) NSString *userDescription;
@property (strong, nonatomic) NSArray  *followers;
@property (strong, nonatomic) NSArray  *following;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSDate   *updatedAt;
@property (strong, nonatomic) NSDate   *createdAt;

@end
