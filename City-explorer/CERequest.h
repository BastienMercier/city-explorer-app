//
//  CERequest.h
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLSApi.h"

@interface CERequest : NSObject
typedef void (^api_request_handler_t)(NSData* data, NSError *error);

@property (strong, nonatomic) NSString      *endPoint;
@property (strong, nonatomic) NSString      *method;
@property (strong, nonatomic) NSDictionary  *header;
@property (strong, nonatomic) NSDictionary  *body;
@property (strong, nonatomic) NSString      *apikey;

- (void)setBlock:(api_request_handler_t)block;
- (api_request_handler_t)block;

- (void)resume;

@end
