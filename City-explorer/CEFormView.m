//
//  CEFormView.m
//  City-explorer
//
//  Created by theo damaville on 12/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEFormView.h"

@interface CEFormView(){
    BOOL _didUpdateConstraints;
}

@end

@implementation CEFormView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.7];
        [self addSubview:self.header];
        [self addSubview:self.headerTitle];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (UIView *)header {
    if (! _header) {
        _header = [UIView new];
        _header.translatesAutoresizingMaskIntoConstraints = NO;
        _header.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.9];
    }
    return _header;
}

- (UILabel *)headerTitle {
    if (! _headerTitle) {
        _headerTitle = [UILabel new];
        _headerTitle.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _headerTitle;
}

- (void)setConstraintsForHeader {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_header
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0f constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_header
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_header
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0f constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_header
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0f constant:20.0]];
}

- (void)setConstraintsForHeaderTitle {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_headerTitle
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_header
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_headerTitle
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_header
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)updateConstraints {
    if (! _didUpdateConstraints) {
        _didUpdateConstraints = YES;
        [self setConstraintsForHeader];
        [self setConstraintsForHeaderTitle];
    }
    [super updateConstraints];
}

@end
