//
//  CEEventDetailViewController.m
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEEventDetailViewController.h"
#import "CETchatController.h"

@interface CEEventDetailViewController () {
	CETchatController		*_tchatController;
}

@end

@implementation CEEventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	_tchatController = [CETchatController new];
	_contentView.menu.delegate = self;
	_tchatController.table = _contentView.tchat.tchatView;
	_contentView.tchat.tchatView.delegate   = _tchatController;
	_contentView.tchat.tchatView.dataSource = _tchatController;
	_contentView.tchat.message.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object: nil];
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
	[[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];
}

- (void)loadView {
	_contentView = [CEMasterEventDetailView new];
	self.view = _contentView;
}

#pragma mark - menu
- (void)didDescriptionSelected {
	[_contentView slideDescriptionView:kCEDirectionRight];
}

- (void)didMessageSelected {
	[_contentView slideDescriptionView:kCEDirectionLeft];
}

#pragma mark - keyboard

- (void)keyboardWillDisappear:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    [_contentView.tchat moveTchatTextDown:keyboardFrameBeginRect.size.height];
}
- (void)keyboardWillAppear:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    [_contentView.tchat moveTchatTextUp:keyboardFrameBeginRect.size.height];
}

@end
