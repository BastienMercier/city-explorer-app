//
//  CEProfilView.h
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CETopProfilView.h"
#import "CEBotProfilView.h"

@interface CEProfilView : UIView

@property (strong, nonatomic) CETopProfilView *topView;
@property (strong, nonatomic) CEBotProfilView *botView;

@end
