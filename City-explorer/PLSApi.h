//
//  PLSApi.h
//  AsyncConnexion
//
//  Created by Administrateur on 10/10/13.
//  Copyright (c) 2013 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CERequest.h"
@class CERequest;
@interface PLSApi : NSObject

/// callback with returned data sent by server in JSON
typedef void (^api_request_handler_t)(NSData* data, NSError *error);


- (void)requestWithRequest:(CERequest *)request callback:(api_request_handler_t)callback;
/**
 * Return the api singleton
 */
+ (PLSApi*) api;
@end
