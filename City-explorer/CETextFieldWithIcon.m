//
//  CETextFieldWithIcon.m
//  City-explorer
//
//  Created by theo damaville on 10/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CETextFieldWithIcon.h"

@interface CETextFieldWithIcon(){
    BOOL _didUpdatedConstraints;
    CGSize imgSize;
}

@end

@implementation CETextFieldWithIcon

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        imgSize = CGSizeMake(0.7, 0.7);
        [self addSubview:self.iconView];
        [self addSubview:self.textView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame imageSize:(CGSize)aImgSize{
    if (self = [super initWithFrame:frame]) {
        imgSize = aImgSize;
        [self addSubview:self.iconView];
        [self addSubview:self.textView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [UIImageView new];
        _iconView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _iconView;
}

- (UITextField *)textView {
    if (!_textView) {
        _textView = [UITextField new];
        _textView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _textView;
}

- (void)setConstraints {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_iconView
                                                    attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_iconView
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:imgSize.height constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_iconView
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:imgSize.width constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_iconView
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:10]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_textView
                                                    attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_textView
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_iconView
                                                    attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_textView
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_textView
                                                    attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:0]];
}

- (void)updateConstraints {
    if (! _didUpdatedConstraints) {
        _didUpdatedConstraints = YES;
        [self setConstraints];
    }
    [super updateConstraints];
}

@end
