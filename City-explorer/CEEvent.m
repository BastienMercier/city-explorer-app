//
//  CEEvent.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEEvent.h"

@implementation CEEvent

- (instancetype)initWithDictionary:(NSDictionary *)event;
{
	self = [super init];
	if (self) {
		//
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	if(!self) {
		return nil;
	}
	
	self.owner = [aDecoder decodeObjectForKey:@"owner"];
	self.eventId = [aDecoder decodeObjectForKey:@"eventId"];
	self.city = [aDecoder decodeObjectForKey:@"city"];
	self.adress = [aDecoder decodeObjectForKey:@"adress"];
	self.date = [aDecoder decodeObjectForKey:@"date"];
	self.age = [aDecoder decodeObjectForKey:@"age"];
	self.gender = [aDecoder decodeObjectForKey:@"gender"];
	self.name = [aDecoder decodeObjectForKey:@"name"];
	self.EventDescription = [aDecoder decodeObjectForKey:@"EventDescription"];
	self.participants = [aDecoder decodeObjectForKey:@"participants"];
	self.updatedAt = [aDecoder decodeObjectForKey:@"updatedAt"];
	self.createdAt = [aDecoder decodeObjectForKey:@"createdAt"];
	self.rank = [aDecoder decodeIntegerForKey:@"rank"];
	self.active = [aDecoder decodeBoolForKey:@"active"];
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:self.owner forKey:@"owner"];
	[coder encodeObject:self.eventId forKey:@"eventId"];
	[coder encodeObject:self.city forKey:@"city"];
	[coder encodeObject:self.adress forKey:@"adress"];
	[coder encodeObject:self.date forKey:@"date"];
	[coder encodeObject:self.age forKey:@"age"];
	[coder encodeObject:self.name forKey:@"name"];
	[coder encodeObject:self.EventDescription forKey:@"EventDescription"];
	[coder encodeObject:self.participants forKey:@"participants"];
	[coder encodeInteger:self.rank forKey:@"rank"];
	[coder encodeBool:self.active forKey:@"active"];
	[coder encodeObject:self.updatedAt forKey:@"updatedAt"];
	[coder encodeObject:self.createdAt forKey:@"createdAt"];
}

- (NSString *)description {
	return [NSString stringWithFormat:@"<%@> id:%@ name:%@",[self class], _eventId, _name];
}

@end
