//
//  CETextFieldWithIcon.h
//  City-explorer
//
//  Created by theo damaville on 10/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CETextFieldWithIcon : UIView

@property (strong, nonatomic) UITextField *textView;
@property (strong, nonatomic) UIImageView *iconView;


- (instancetype)initWithFrame:(CGRect)frame imageSize:(CGSize)aImgSize;
@end
