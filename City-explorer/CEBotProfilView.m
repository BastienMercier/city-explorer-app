//
//  CEBotProfilView.m
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEBotProfilView.h"

@interface CEBotProfilView ()
{
    BOOL _didUpdateConstraint;
}
@end
@implementation CEBotProfilView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.bgImage];
        [self addSubview:self.tableEvents];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (UIImageView *)bgImage {
    if (! _bgImage) {
        _bgImage = [UIImageView new];
        _bgImage.translatesAutoresizingMaskIntoConstraints = NO;
        _bgImage.image = [UIImage imageNamed:@"backgound_profil_blur"];
    }
    return _bgImage;
}

- (void)setConstraintForBackground {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bgImage
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bgImage
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bgImage
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_bgImage
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:0]];
}

- (void)updateConstraints {
    if (! _didUpdateConstraint) {
        [self setConstraintForBackground];
        _didUpdateConstraint = YES;
    }
    [super updateConstraints];
}

@end
