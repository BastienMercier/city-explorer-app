//
//  CEFormView.h
//  City-explorer
//
//  Created by theo damaville on 12/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEFormView : UIView

@property (strong, nonatomic) UIView *header;
@property (strong, nonatomic) UILabel *headerTitle;

@end
