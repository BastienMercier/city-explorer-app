//
//  CEProfilView.m
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEProfilView.h"
@interface CEProfilView(){
    BOOL _didUpdatedConstraints;
}
@end

@implementation CEProfilView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blueColor];
        [self addSubview:self.topView];
        [self addSubview:self.botView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (CETopProfilView *)topView {
    if (! _topView) {
        _topView = [CETopProfilView new];
        _topView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _topView;
}

- (CEBotProfilView *)botView {
    if (! _botView) {
        _botView = [CEBotProfilView new];
        _botView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _botView;
}

- (void)constraintForTop {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:64]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0.33 constant:0]];
}

- (void)constraintForBot {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_botView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_topView
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_botView
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_botView
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_botView
                                                    attribute:NSLayoutAttributeBottom
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:0]];
}

- (void)updateConstraints {
    if (! _didUpdatedConstraints) {
        _didUpdatedConstraints = YES;
        [self constraintForTop];
        [self constraintForBot];
    }
    [super updateConstraints];
}
@end
