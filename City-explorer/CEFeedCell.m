//
//  FeedCell.m
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEFeedCell.h"
#import "UIFont+AppFont.h"
#import "UIColor+CustomColor.h"

#define cell_size (274/2)
#define cell_padding 8.0f
#define cell_blue_color [UIColor colorWithR:130 G:213 B:250 alpha:1]
@interface CEFeedCell(){
    BOOL _didUpdateConstraints;
}

@end

@implementation CEFeedCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.calendarIcon];
        [self.contentView addSubview:self.date];
        [self.contentView addSubview:self.distance];
        [self.contentView addSubview:self.locationIcon];
        [self.contentView addSubview:self.city];
        [self.contentView addSubview:self.owner];
        [self.contentView addSubview:self.title];
        [self.contentView addSubview:self.eventDescription];
        [self setStars];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEvent:(CEEvent *)event {
    _event = event;
    _date.text = event.date;
    _date.text = [NSString stringWithFormat:@"%dm",0];
    _city.text = event.city;
    _owner.text = event.owner.firstname;
    _title.text = event.name;
    _eventDescription.text = event.EventDescription;
}

- (UIImageView *)calendarIcon {
    if (!_calendarIcon) {
        _calendarIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_calendar"]];
        _calendarIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _calendarIcon;
}

- (UIImageView *)locationIcon {
    if (!_locationIcon) {
        _locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_location"]];
        _locationIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _locationIcon;
}

- (UILabel *)date {
    if (!_date) {
        _date = [UILabel new];
        _date.text = @"17 novembre";
        _date.font = [UIFont appFontLightOfSize:14];
        _date.textColor = [UIColor whiteColor];
        _date.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _date;
}

- (UILabel *)distance {
    if (!_distance) {
        _distance = [UILabel new];
        _distance.translatesAutoresizingMaskIntoConstraints = NO;
        _distance.font = [UIFont appFontLightOfSize:14];
        _distance.textColor = [UIColor whiteColor];
        _distance.text = @"500m";
    }
    return _distance;
}

- (UILabel *)city {
    if (!_city) {
        _city = [UILabel new];
        _city.font = [UIFont appFontLightOfSize:16];
        _city.textColor = [UIColor colorWithR:240 G:240 B:240 A:1];
        _city.translatesAutoresizingMaskIntoConstraints = NO;
        _city.text = @"Le Havre";
    }
    return _city;
}

- (UILabel *)owner {
    if (!_owner) {
        _owner = [UILabel new];
        _owner.translatesAutoresizingMaskIntoConstraints = NO;
        _owner.font = [UIFont appFontOfSize:18];
        _owner.textColor = [UIColor whiteColor];
        _owner.text = @"Matéooo";
    }
    return _owner;
}

- (UILabel *)title {
    if (! _title) {
        _title = [UILabel new];
        _title.translatesAutoresizingMaskIntoConstraints = NO;
        _title.text = @"Soirée bière !";
        _title.font = [UIFont appFontLightOfSize:24];
        _title.textColor = [UIColor colorWithR:130 G:213 B:250 A:1];
        
    }
    return _title;
}

- (UILabel *)eventDescription {
    if (! _eventDescription) {
        _eventDescription = [UILabel new];
        _eventDescription.translatesAutoresizingMaskIntoConstraints = NO;
        _eventDescription.font = [UIFont appFontLightOfSize:14];
        _eventDescription.text = @"Soirée entre amis à la maison...";
        _eventDescription.textColor = [UIColor colorWithR:130 G:213 B:250 A:1];
    }
    return _eventDescription;
}

- (void)setStars {
    int rating = 3;
    NSMutableArray *stars = [NSMutableArray new];
    for (int i = 0; i < 5; i++) {
        UIImage *starImg;
        if (i < rating) {
            starImg = [UIImage imageNamed:@"star_icon"];
        } else {
            starImg = [UIImage imageNamed:@"star_empty_icon"];
        }
        UIImageView *iView = [[UIImageView alloc] initWithImage:starImg];
        [self.contentView addSubview:iView];
        iView.translatesAutoresizingMaskIntoConstraints = NO;
        [stars addObject:iView];
    }
    _stars = stars;
}

- (void)setCalendarIconConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_calendarIcon
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeLeft
                                                                multiplier:1.0 constant:cell_padding]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_calendarIcon
                                                                attribute:NSLayoutAttributeBottom
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0 constant:-cell_padding]];
}

- (void)setDateConstaints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_calendarIcon
                                                                attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0 constant:0.0f]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_calendarIcon
                                                                attribute:NSLayoutAttributeRight
                                                                multiplier:1.0 constant:3.0f]];
}

- (void)setDistanceConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_distance
                                                                attribute:NSLayoutAttributeRight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeRight
                                                                multiplier:1.0 constant:-cell_padding]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_distance
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_date
                                                                attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0 constant:0.0]];
}

- (void)setLocationIconConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                                                attribute:NSLayoutAttributeRight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_distance
                                                                attribute:NSLayoutAttributeLeft
                                                                multiplier:1.0 constant:-3.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_date
                                                                attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0 constant:0.0]];
    
}

- (void)setCityConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                                attribute:NSLayoutAttributeBottom
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_distance
                                                                attribute:NSLayoutAttributeTop
                                                                multiplier:1.0 constant:-3.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                                attribute:NSLayoutAttributeRight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_distance
                                                                attribute:NSLayoutAttributeRight
                                                                multiplier:1.0 constant:0]];
    
}

- (void)setOwnerConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_owner
                                                                attribute:NSLayoutAttributeTop
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeTop
                                                                multiplier:1.0 constant:cell_padding]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_owner
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeLeft
                                                                multiplier:1.0 constant:cell_padding]];
}

- (void)setTitleConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_title
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_title
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeCenterY
                                                                multiplier:0.8 constant:0]];
}

- (void)setEventDescriptionConstraints {
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_eventDescription
                                                                attribute:NSLayoutAttributeTop
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_title
                                                                attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0 constant:3.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_eventDescription
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0 constant:0]];
}

- (void)setStarsConstraints {
    // initial state
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_stars[0]
                                                                attribute:NSLayoutAttributeTop
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_owner
                                                                attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0 constant:1.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_stars[0]
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeLeft
                                                                multiplier:1.0 constant:cell_padding]];
    
    for (int i = 1; i < 5; i++) {
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_stars[i]
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_stars[i-1]
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0 constant:0.0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_stars[i]
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_stars[i-1]
                                                                     attribute:NSLayoutAttributeRight
                                                                    multiplier:1.0 constant:1.0]];
    }
}

- (void)updateConstraints {
    if (!_didUpdateConstraints) {
        [self setCalendarIconConstraints];
        [self setDateConstaints];
        [self setDistanceConstraints];
        [self setLocationIconConstraints];
        [self setCityConstraints];
        [self setOwnerConstraints];
        [self setTitleConstraints];
        [self setEventDescriptionConstraints];
        [self setStarsConstraints];
        _didUpdateConstraints = YES;
    }
    [super updateConstraints];
}

@end
