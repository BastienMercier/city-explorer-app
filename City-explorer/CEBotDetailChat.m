//
//  CEBotDetailChat.m
//  City-explorer
//
//  Created by Administrateur on 21/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEBotDetailChat.h"
#import "UIColor+CustomColor.h"

@interface  CEBotDetailChat(){
	BOOL _didUpdateConstraints;
}
@end

@implementation CEBotDetailChat

- (instancetype)init {
	return [self initWithParticipants:@[@"Anne",@"Claudie"]];
}

- (instancetype)initWithParticipants:(NSArray *)participants
{
	self = [super init];
	if (self) {
		self.backgroundColor = [UIColor whiteColor];
		self.participants = participants;
		self.tchatViews = [@{} mutableCopy];
		[self addSubview:self.scrollingParticipants];
		[self addSubview:self.tchatView];
		[self addSubview:self.greyView];
		[self.greyView addSubview:self.message];
		[self.greyView addSubview:self.sendButton];
		
		[self setNeedsUpdateConstraints];
        
	}
	return self;
}

- (UIScrollView *)scrollingParticipants {
	if (! _scrollingParticipants) {
		_scrollingParticipants = [UIScrollView new];
		_scrollingParticipants.translatesAutoresizingMaskIntoConstraints = NO;
        _scrollingParticipants.backgroundColor = [self bgColor];
	}
	
	return _scrollingParticipants;
}

- (UITableView *)tchatView {
	if (! _tchatView) {
		_tchatView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
		_tchatView.translatesAutoresizingMaskIntoConstraints = NO;
        _tchatView.backgroundColor = [self bgColor];
        _tchatView.separatorStyle = UITableViewCellSeparatorStyleNone;
	}
	return _tchatView;
}

- (UIView *)greyView {
	if (!_greyView) {
		_greyView = [UIView new];
		_greyView.translatesAutoresizingMaskIntoConstraints = NO;
		_greyView.backgroundColor = [UIColor lightTextColor];
	}
	return _greyView;
}

- (UITextView *)message {
	if (!_message) {
		_message = [UITextView new];
		_message.layer.cornerRadius = 3.0;
		_message.layer.masksToBounds = YES;
		_message.text = @"Votre commentaire...";
		_message.font = [UIFont fontWithName:@"Avenir" size:16];
		_message.translatesAutoresizingMaskIntoConstraints = NO;
	}
	return _message;
}

- (UIButton *)sendButton {
	if (!_sendButton) {
		_sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
		_sendButton.translatesAutoresizingMaskIntoConstraints = NO;
		[_sendButton setTitle:@"Envoyer" forState:UIControlStateNormal];
		[_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		_sendButton.backgroundColor = [UIColor blueColor];
		_sendButton.layer.cornerRadius = 2.0f;
		_sendButton.layer.masksToBounds = YES;
	}
	return _sendButton;
}

- (void)setConstraintBottomView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_greyView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_greyView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.0 constant:45]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_greyView
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeLeft
													multiplier:1.0 constant:0.0]];
    _grayViewConstraintY = [NSLayoutConstraint constraintWithItem:_greyView
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0 constant:0.0];
	[self addConstraint:_grayViewConstraintY];
	CGFloat padding = 6.0f;
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_message
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeWidth
														 multiplier:0.7 constant:0.0]];
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_message
														  attribute:NSLayoutAttributeLeft
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeLeft
														 multiplier:1.0f constant:padding]];
    [_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_message
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_greyView
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0f constant:0.0]];
	
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_message
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeHeight
														 multiplier:1.0 constant:-padding*2]];
	
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_sendButton
														  attribute:NSLayoutAttributeCenterY
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeCenterY
														 multiplier:1.0 constant:0.0]];
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_sendButton
														  attribute:NSLayoutAttributeLeft
														  relatedBy:NSLayoutRelationEqual
															 toItem:_message
														  attribute:NSLayoutAttributeRight
														 multiplier:1.0 constant:padding]];
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_sendButton
														  attribute:NSLayoutAttributeRight
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeRight
														 multiplier:1.0 constant:-padding]];
	[_greyView addConstraint:[NSLayoutConstraint constraintWithItem:_sendButton
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:_greyView
														  attribute:NSLayoutAttributeHeight
														 multiplier:1.0 constant:-padding*2]];
}

- (void)setConstraintForScrollView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_scrollingParticipants
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.1 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_scrollingParticipants
													 attribute:NSLayoutAttributeCenterX
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterX
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_scrollingParticipants
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeTop
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_scrollingParticipants
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0.0]];
}

- (void)setConstraintForTchat {
	_bottomConstraintForTableTchat = [NSLayoutConstraint constraintWithItem:_tchatView
								 attribute:NSLayoutAttributeBottom
								 relatedBy:NSLayoutRelationEqual
									toItem:self
								 attribute:NSLayoutAttributeBottom
								multiplier:1.0 constant:-45.0];
	[self addConstraint:_bottomConstraintForTableTchat];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchatView
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:self.scrollingParticipants
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchatView
													 attribute:NSLayoutAttributeCenterX
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterX
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchatView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0.0]];
	
}

- (void)updateConstraints {
	if (! _didUpdateConstraints) {
		_didUpdateConstraints = YES;
		[self setConstraintForScrollView];
		[self setConstraintForTchat];
		[self setConstraintBottomView];
	}
    [super updateConstraints];
}

- (void)layoutSubviews {
	[super layoutSubviews];
	[self buildScrollView];
	[self sizeScrollView];
}

#pragma mark - tools scrollview
- (void)buildScrollView {
	CGFloat width = 0.0f;
	for (int i = 0; i < self.participants.count; i++) {
		UIView *v = [[UIView alloc] initWithFrame:CGRectMake(width, 0, self.bounds.size.width/3.0f, _scrollingParticipants.bounds.size.height)];
		v.backgroundColor = [UIColor lightTextColor];
		[self.scrollingParticipants addSubview:v];
        
        UIButton *b = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        b.frame = v.bounds;
        [b setTitle:_participants[i] forState:UIControlStateNormal];
        b.titleLabel.textAlignment = NSTextAlignmentCenter;
        b.tag = i;
        [v addSubview:b];
        width += self.bounds.size.width/3.0f +1;
	}
}

- (void)sizeScrollView {
	CGSize contentSize;
	CGFloat width = 0.0f;
	for (int i = 0; i < self.participants.count; i++) {
		width += self.bounds.size.width/3.0f + 1;
	}
	contentSize = CGSizeMake(width, _scrollingParticipants.bounds.size.height);
	_scrollingParticipants.contentSize = contentSize;
}

- (void)moveTchatTextUp:(CGFloat)offset {
	[UIView animateWithDuration:0.3 animations:^{
		_grayViewConstraintY.constant -= offset;
		[self layoutIfNeeded];
	}];
}

- (void)moveTchatTextDown:(CGFloat)offset {
	[UIView animateWithDuration:0.3 animations:^{
		_grayViewConstraintY.constant += offset;
		[self layoutIfNeeded];
	}];
}

- (UIColor *)bgColor {
    return [UIColor colorWithR:48 G:48 B:48 A:1];
}
@end
