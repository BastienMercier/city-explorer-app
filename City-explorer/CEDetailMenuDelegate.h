//
//  CEDetailMenuDelegate.h
//  City-explorer
//
//  Created by Administrateur on 18/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CEDetailMenuDelegate <NSObject>

@required
- (void)didMessageSelected;
- (void)didDescriptionSelected;

@end
