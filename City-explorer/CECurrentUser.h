//
//  CECurrentUser.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEUser.h"

@interface CECurrentUser : NSObject

/**
 *  Singleton
 *
 *  @return the current user of the app
 */
+ (CECurrentUser *)sharedUser;
    
@property (strong, nonatomic) CEUser    *currentUser;
@property (strong, nonatomic) NSString  *token;

@end
