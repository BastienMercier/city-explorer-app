//
//  CETopProfilView.m
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CETopProfilView.h"
#import "UIColor+CustomColor.h"
#import "UIFont+AppFont.h"
#import "CECurrentUser.h"

@interface CETopProfilView () {
    BOOL _didUpdatedConstraint;
}
@end

@implementation CETopProfilView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithR:92 G:92 B:92 A:1];
        [self addSubview:self.picture];
        [self addSubview:self.events];
        [self addSubview:self.follower];
        [self addSubview:self.userName];
        [self addSubview:self.followingButton];
        [self addSubview:self.followCount];
        [self addSubview:self.followLabel];
        [self addSubview:self.eventLabel];
        [self addSubview:self.eventCount];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)setData:(CEUser *)currentUser {
	self.picture.image = currentUser.picture;
	self.userName.text = currentUser.firstname;
	self.followCount.text = [NSString stringWithFormat:@"%d",currentUser.followers.count];
	if ([currentUser.userId isEqualToString:[CECurrentUser sharedUser].currentUser.userId]) {
		_followingButton.hidden = YES;
	}
}

- (UIImageView *)picture {
    if (! _picture) {
        _picture = [UIImageView new];
        _picture.translatesAutoresizingMaskIntoConstraints = NO;
        _picture.image = [UIImage imageNamed:@"sample_bastien"];
        _picture.backgroundColor = [UIColor blueColor];
        _picture.layer.borderColor = [self colorForBoders].CGColor;
        _picture.layer.borderWidth = 1;
    }
    return _picture;
}

- (UIView *)events {
    if (! _events) {
        _events = [UIView new];
        _events.translatesAutoresizingMaskIntoConstraints = NO;
        _events.layer.borderColor = [self colorForBoders].CGColor;
        _events.backgroundColor = [UIColor colorWithR:80 G:80 B:80 A:1];
        _events.layer.borderWidth = 1;
    }
    return _events;
}

- (UIView *)follower {
    if (! _follower) {
        _follower = [UIView new];
        _follower.translatesAutoresizingMaskIntoConstraints = NO;
        _follower.layer.borderColor = [self colorForBoders].CGColor;
        _follower.backgroundColor = _events.backgroundColor;
        _follower.layer.borderWidth = 1;
    }
    return _follower;
}

- (UILabel *)userName {
    if (!_userName) {
        _userName = [UILabel new];
        _userName.translatesAutoresizingMaskIntoConstraints = NO;
        _userName.text = @"Bastien";
        _userName.font = [UIFont appFontOfSize:24];
        _userName.textColor = [UIColor colorWithR:200 G:200 B:200 A:1];
    }
    return _userName;
}

- (UIButton *)followingButton {
    if (! _followingButton) {
        _followingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _followingButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_followingButton setTitle:@"Suivre" forState:UIControlStateNormal];
        _followingButton.titleLabel.font = [UIFont appFontOfSize:20];
        [_followingButton setTitleColor:[UIColor tabBarColorIndicator] forState:UIControlStateNormal];
        _followingButton.layer.borderColor = [UIColor tabBarColorIndicator].CGColor;
        _followingButton.layer.borderWidth = 1;
        _followingButton.layer.masksToBounds = YES;
        _followingButton.layer.cornerRadius = 5;
    }
    return _followingButton;
}

- (UILabel *)eventCount {
    if (! _eventCount) {
        _eventCount = [UILabel new];
        _eventCount.translatesAutoresizingMaskIntoConstraints = NO;
        _eventCount.text = @"0";
        _eventCount.font = [UIFont appFontOfSize:22];
        _eventCount.textColor = [UIColor whiteColor];
    }
    return _eventCount;
}

- (UILabel *)followCount {
    if (! _followCount) {
        _followCount = [UILabel new];
        _followCount.translatesAutoresizingMaskIntoConstraints = NO;
        _followCount.text = @"4";
        _followCount.font = [UIFont appFontOfSize:22];
        _followCount.textColor = [UIColor whiteColor];
    }
    return _followCount;
}

- (UILabel *)followLabel {
    if (! _followLabel) {
        _followLabel = [UILabel new];
        _followLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _followLabel.text = @"followers";
        _followLabel.font = [UIFont appFontOfSize:17];
        _followLabel.textColor = [UIColor whiteColor];
    }
    return _followLabel;
}

- (UILabel *)eventLabel {
    if (! _eventLabel) {
        _eventLabel = [UILabel new];
        _eventLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _eventLabel.text = @"soirées";
        _eventLabel.font = [UIFont appFontOfSize:17];
        _eventLabel.textColor = [UIColor whiteColor];
    }
    return _eventLabel;
}

- (void)setConstraintForPicture {
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_picture
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_picture
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:0.6 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_picture
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.6 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_picture
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.6 constant:0]];
}

- (void)setConstraintForEvents {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_events
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.55 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_events
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.66 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_events
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_events
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.3 constant:0]];
}

- (void)setConstraintForFollower; {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_follower
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.55 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_follower
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.66 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_follower
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_follower
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.3 constant:0]];
 
}

- (void)setConstraintForName {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_userName
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_picture
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1. constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_userName
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1. constant:0]];
}

- (void)setConstraintForFollowing {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followingButton
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_userName
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1. constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followingButton
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1. constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followingButton
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.3 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followingButton
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:30]];
}

- (void)setConstraintForFollowCount {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followCount
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_follower
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:0.9 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followCount
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_follower
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForEventCount {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventCount
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_events
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:0.9 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventCount
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_events
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
}
- (void)setConstraintForFollow {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followLabel
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_followCount
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1 constant:-5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_followLabel
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_follower
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForEvent {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventLabel
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventCount
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1 constant:-5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventLabel
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_events
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    
}

- (void)updateConstraints {
    if (! _didUpdatedConstraint) {
        _didUpdatedConstraint = YES;
        [self setConstraintForPicture];
        [self setConstraintForEvents];
        [self setConstraintForFollower];
        [self setConstraintForName];
        [self setConstraintForFollowing];
        [self setConstraintForFollowCount];
        [self setConstraintForEventCount];
        [self setConstraintForFollow];
        [self setConstraintForEvent];
    }
    [super updateConstraints];
}

- (UIColor *)colorForBoders; {
    return [UIColor colorWithR:0 G:122 B:255 A:1];
}
@end
