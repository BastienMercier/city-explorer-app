//
//  CECurrentUser.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CECurrentUser.h"

static CECurrentUser *sharedUser;
@implementation CECurrentUser

+ (void)initialize {
    if (self == [CECurrentUser class]) {
        sharedUser = [[CECurrentUser alloc] init];
    }
}

+ (CECurrentUser *)sharedUser {
    return sharedUser;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	if(!self) {
		return nil;
	}
	
	self.currentUser = [aDecoder decodeObjectForKey:@"currentUser"];
	self.token = [aDecoder decodeObjectForKey:@"token"];
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:self.currentUser forKey:@"currentUser"];
	[coder encodeObject:self.token forKey:@"token"];
}

- (NSString *)description {
	return [NSString stringWithFormat:@"token:%@ id:%@ name:%@",_token, _currentUser.userId, _currentUser.firstname];
}

@end
