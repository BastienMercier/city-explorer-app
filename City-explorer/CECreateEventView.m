//
//  CECreateEventView.m
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CECreateEventView.h"
#import "UIFont+AppFont.h"
#import "UIColor+CustomColor.h"

@interface CECreateEventView() {
    BOOL _didUpdateConstraints;
}

@end

@implementation CECreateEventView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"create_bg"]];
        [self addSubview:self.infosCity];
        [self addSubview:self.city];
        [self addSubview:self.date];
        [self addSubview:self.infosOther];
        
        [self addSubview:self.ageIcon];
        [self addSubview:self.ageMoyen];
        [self addSubview:self.ageDisplay];
        [self addSubview:self.ageSlider];
        
        [self addSubview:self.sexLabel];
        [self addSubview:self.sexMale];
        [self addSubview:self.sexFemale];
        [self addSubview:self.sexBoth];
        
        [self addSubview:self.descView];
        [self addSubview:self.descIcon];
        [self addSubview:self.descEvent];
        [self addSubview:self.descArrow];
        
        [self addSubview:self.validate];
        [self addSubview:self.indicator];
        
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (CEFormView *)infosCity {
    if (! _infosCity) {
        _infosCity = [CEFormView new];
        _infosCity.translatesAutoresizingMaskIntoConstraints = NO;
        _infosCity.headerTitle.text = @"Informations pratiques";
        _infosCity.headerTitle.textColor = [UIColor whiteColor];
        _infosCity.headerTitle.font = [UIFont appFontOfSize:18];
    }
    return _infosCity;
}

- (CEFormView *)infosOther {
    if (! _infosOther) {
        _infosOther = [CEFormView new];
        _infosOther.translatesAutoresizingMaskIntoConstraints = NO;
        _infosOther.headerTitle.text = @"Informations diverses";
        _infosOther.headerTitle.textColor = [UIColor whiteColor];
        _infosOther.headerTitle.font = [UIFont appFontOfSize:18];
    }
    return _infosOther;
}

- (CETextFieldWithIcon *)city {
    if (! _city) {
        _city = [CETextFieldWithIcon new];
        _city.translatesAutoresizingMaskIntoConstraints = NO;
        _city.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
        _city.iconView.image = [UIImage imageNamed:@"city_icone"];
		_city.textView.textColor = [UIColor whiteColor];
		_city.textView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Ville" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    return _city;
}

- (CETextFieldWithIcon *)date {
    if (! _date) {
        _date = [CETextFieldWithIcon new];
        _date.translatesAutoresizingMaskIntoConstraints = NO;
        _date.backgroundColor = _city.backgroundColor;
        _date.iconView.image = [UIImage imageNamed:@"icon_calendar"];
		_date.textView.textColor = [UIColor whiteColor];
		_date.textView.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];;
    }
    return _date;
}

- (UIImageView *)ageIcon {
    if (! _ageIcon) {
        _ageIcon = [UIImageView new];
        _ageIcon.translatesAutoresizingMaskIntoConstraints = NO;
        _ageIcon.image = [UIImage imageNamed:@"gateau_icon"];
    }
    return _ageIcon;
}

- (UILabel *)ageMoyen {
    if (! _ageMoyen) {
        _ageMoyen = [UILabel new];
        _ageMoyen.translatesAutoresizingMaskIntoConstraints = NO;
        _ageMoyen.text = @"Age moyen";
        _ageMoyen.textColor = [UIColor whiteColor];
        _ageMoyen.font = [UIFont appFontOfSize:16];
    }
    return _ageMoyen;
}

- (UILabel *)ageDisplay {
    if (! _ageDisplay) {
        _ageDisplay = [UILabel new];
        _ageDisplay.translatesAutoresizingMaskIntoConstraints = NO;
        _ageDisplay.text = @"15 ans";
        _ageDisplay.font = _ageMoyen.font;
        _ageDisplay.textColor = _ageMoyen.textColor;
    }
    return _ageDisplay;
}

- (UISlider *)ageSlider {
    if (! _ageSlider) {
        _ageSlider = [UISlider new];
        _ageSlider.translatesAutoresizingMaskIntoConstraints = NO;
        _ageSlider.minimumValue = 15;
        _ageSlider.maximumValue = 55;
    }
    return _ageSlider;
}

- (UILabel *)sexLabel {
    if (! _sexLabel) {
        _sexLabel = [UILabel new];
        _sexLabel.text = @"Sexe des invités";
        _sexLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _sexLabel.textColor = [UIColor whiteColor];
        _sexLabel.font = [UIFont appFontOfSize:19];
    }
    return _sexLabel;
}

- (UIImageView *)sexBoth {
    if (! _sexBoth) {
        _sexBoth = [UIImageView new];
        _sexBoth.translatesAutoresizingMaskIntoConstraints = NO;
        _sexBoth.image = [UIImage imageNamed:@"sex_both_icon"];
    }
    return _sexBoth;
}

- (UIImageView *)sexMale {
    if (! _sexMale) {
        _sexMale = [UIImageView new];
        _sexMale.translatesAutoresizingMaskIntoConstraints = NO;
        _sexMale.image = [UIImage imageNamed:@"sex_male_icon"];
    }
    return _sexMale;
}

- (UIImageView *)sexFemale {
    if (! _sexFemale) {
        _sexFemale = [UIImageView new];
        _sexFemale.translatesAutoresizingMaskIntoConstraints = NO;
        _sexFemale.image = [UIImage imageNamed:@"sex_female_icon"];
    }
    return _sexFemale;
}

- (UIView *)descView {
    if (! _descView) {
        _descView = [UIView new];
        _descView.translatesAutoresizingMaskIntoConstraints = NO;
        _descView.backgroundColor = _infosOther.backgroundColor;
    }
    return _descView;
}

- (UIImageView *)descIcon {
    if (! _descIcon) {
        _descIcon = [UIImageView new];
        _descIcon.translatesAutoresizingMaskIntoConstraints = NO;
        _descIcon.image = [UIImage imageNamed:@"leather_icon"]; // 31*44
    }
    return _descIcon;
}

- (UILabel *)descEvent {
    if (! _descEvent) {
        _descEvent = [UILabel new];
        _descEvent.translatesAutoresizingMaskIntoConstraints = NO;
        _descEvent.text = @"Description";
        _descEvent.textColor = [UIColor whiteColor];
        _descEvent.font = [UIFont appFontOfSize:19];
    }
    return _descEvent;
}

- (UIImageView *)descArrow {
    if (! _descArrow) {
        _descArrow = [UIImageView new];
        _descArrow.translatesAutoresizingMaskIntoConstraints = NO;
        _descArrow.image = [UIImage imageNamed:@"arrow_icon"]; // 25*41
    }
    return _descArrow;
}

- (UIButton *)validate {
    if (! _validate) {
        _validate = [UIButton buttonWithType:UIButtonTypeCustom];
        _validate.translatesAutoresizingMaskIntoConstraints = NO;
        [_validate setTitle:@"Valider" forState:UIControlStateNormal];
        _validate.layer.borderWidth = 1;
        _validate.layer.borderColor = [UIColor whiteColor].CGColor;
        _validate.layer.masksToBounds = YES;
        _validate.layer.cornerRadius = 5;
    }
    return _validate;
}

- (UIView *)indicator {
    if (! _indicator) {
        _indicator = [UIView new];
        _indicator.backgroundColor = [UIColor tabBarColorIndicator];
        _indicator.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _indicator;
}

- (void)setConstraintForInfoCity {
    
    CGFloat headerSize = 20;
    CGFloat nbItem = 2;
    CGFloat itemInterspace = 5;
    CGFloat headerInterspace = 10;
    CGFloat itemSize = 40;
    CGFloat size = headerSize + nbItem*(itemInterspace-1) + headerInterspace*2 + nbItem*itemSize;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosCity
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosCity
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:size]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosCity
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:65.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosCity
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForCity {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self.infosCity
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:-10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:40]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self.infosCity.header
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    
}

- (void)setConstraintForDate {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self.infosCity
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:-10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:40]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_city
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForOtherInfo {
    //CGFloat headerSize = 20;
    CGFloat size = 160;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosOther
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosOther
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:size]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosOther
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:5.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_infosOther
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
 
}

- (void)setConstraintForAgeIcon {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther.header
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:25]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0 constant:25]];
}
- (void)setConstraintForAgeMedium {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageMoyen
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageMoyen
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:5]];
}
- (void)setConstraintForAgeDisplay {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageDisplay
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageDisplay
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:-5]];
}
- (void)setConstraintForAgeSlider {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageDisplay
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageMoyen
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageDisplay
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:-3]];
}

- (void)setConstraintForSexLabel {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexLabel
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:15]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexLabel
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:5]];
}

- (void)setConstraintForSexMale {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexMale
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_sexLabel
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:7]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexMale
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:-10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexMale
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexMale
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
}

- (void)setConstraintForSexFemale {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexFemale
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_sexMale
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexFemale
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexFemale
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexFemale
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
}

- (void)setConstraintForSexBoth {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexBoth
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_sexMale
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexBoth
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexBoth
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_sexBoth
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:45]];
}

- (void)setConstraintForDescView {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descView
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.infosCity
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descView
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:40]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_infosOther
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:5]];
}

- (void)setConstraintForDescIcon {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descIcon
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descIcon
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descIcon
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0 constant:20]];
}

- (void)setConstraintForDescLabel {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descEvent
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descIcon
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descEvent
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForDescArrow {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descArrow
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descArrow
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:-5]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descArrow
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0 constant:20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_descArrow
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0 constant:10]];
}

- (void)setConstraintForValidate {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_validate
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_validate
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_descView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:15]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_validate
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.5 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_validate
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:40]];
}

- (void)setConstraintForIndicator {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:-49.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:3.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.33 constant:0.0]];
}

- (void)updateConstraints {
    if (! _didUpdateConstraints) {
        _didUpdateConstraints = YES;
        [self setConstraintForInfoCity];
        [self setConstraintForCity];
        [self setConstraintForDate];
        [self setConstraintForOtherInfo];
        [self setConstraintForAgeIcon];
        [self setConstraintForAgeMedium];
        [self setConstraintForAgeDisplay];
        [self setConstraintForAgeSlider];
        [self setConstraintForSexLabel];
        [self setConstraintForSexMale];
        [self setConstraintForSexFemale];
        [self setConstraintForSexBoth];
        [self setConstraintForDescView];
        [self setConstraintForDescIcon];
        [self setConstraintForDescLabel];
        [self setConstraintForDescArrow];
        [self setConstraintForValidate];
        [self setConstraintForIndicator];
    }
    [super updateConstraints];
}

@end
