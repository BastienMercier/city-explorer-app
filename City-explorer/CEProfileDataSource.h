//
//  CEProfileDataSource.h
//  City-explorer
//
//  Created by Administrateur on 03/12/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEUser.h"
#import "CEUpdatedDataSource.h"

@interface CEProfileDataSource : NSObject

@property (strong, nonatomic) CEUser *currentUser;
@property (weak, nonatomic) id<CEUpdatedDataSource> delegate;

@end
