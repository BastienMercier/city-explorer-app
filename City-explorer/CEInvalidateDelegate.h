//
//  CEInvalidateDelegate.h
//  tablechat
//
//  Created by Administrateur on 20/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CEInvalidateDelegate <NSObject>

- (void)shouldInvalidate:(id)me;

@end
