//
//  CEFeedDataSource.h
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEUpdatedDataSource.h"
#import <UIKit/UIKit.h>

@interface CEFeedDataSource : NSObject<UITableViewDataSource>

- (instancetype)initWithDelegate:(id<CEUpdatedDataSource>) delegate;

@property (weak, nonatomic) id<CEUpdatedDataSource> delegate;

@end
