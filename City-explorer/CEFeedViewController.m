//
//  CEFeedViewController.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEFeedViewController.h"
#import "CEProfilViewController.h"
#import "CEEventDetailViewController.h"
#import "AppDelegate.h"

@interface CEFeedViewController ()
@property (strong, nonatomic) UIImageView *searchView;
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic) BOOL endDragging;
@end

@implementation CEFeedViewController

- (void)loadView {
	_contentView = [CEFeedView new];
	self.view = _contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // disable blank view
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addSearchButton];
    [self addProfilButton];
    [self configureTableView];
    [self.contentView.searchView.ageSlider addTarget:self action:@selector(handleAgeSlide:) forControlEvents:UIControlEventValueChanged];
    [self.contentView.searchView.distanceSlider addTarget:self action:@selector(handleDistanceSlide:) forControlEvents:UIControlEventValueChanged];
    self.contentView.searchView.date.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_contentView.tableFeed deselectRowAtIndexPath:[_contentView.tableFeed indexPathForSelectedRow] animated:YES];
	[self configureNavigationBar];
}

- (void)configureNavigationBar
{
	self.navigationController.navigationBar.translucent = YES;
	self.navigationController.navigationBar.topItem.title = @"City Explorer";
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithWhite:0.9 alpha:1]];
	[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"feed_bg"]]];
	[self.navigationController.navigationBar setTitleTextAttributes:
	 [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:21],
	  NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
}

- (void)addProfilButton {
    UIImageView *profil = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sample_theo"]];
    profil.frame = CGRectMake(20, 0, 40, 40);
    profil.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfil)];
    [profil addGestureRecognizer:tap];
    profil.tag = 0;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:profil];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (void)addSearchButton {
    _searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loupe_white"]];
    _searchView.frame = CGRectMake(20, 0, 50, 50);
    _searchView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSearch:)];
    [_searchView addGestureRecognizer:tap];
    _searchView.tag = 0;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:_searchView];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)configureTableView {
    _feedDataSource = [[CEFeedDataSource alloc] initWithDelegate:self];
    _contentView.tableFeed.delegate = self;
    _contentView.tableFeed.dataSource = _feedDataSource;
}

#pragma mark - tableView delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 160;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	CEEventDetailViewController *detail = [CEEventDetailViewController new];
	detail.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:detail animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if (_searchView.tag > 0 && !_endDragging) {
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            [self.view layoutIfNeeded];
            [UIView animateWithDuration:0.3 animations:^{
                [self.contentView searchViewOffset:_lastContentOffset-scrollView.contentOffset.y-10];
                [self.contentView.searchView endEditing:YES];
                _searchView.tag = 2;
                [self.view layoutIfNeeded];
            }];
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (_searchView.tag == 2) {
        [self foldSearchView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	_endDragging = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	_endDragging = YES;
    if (_searchView.tag == 2) {
        [self foldSearchView];
    }
}

#pragma mark - datasource

- (void)didUpdateDataSource {
    [_contentView.tableFeed reloadData];
}

- (void)willUpdateDataSource {
    [_contentView.tableFeed reloadData];
}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    [self handleDateTaped:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;
{
    return YES;
}

#pragma mark - user events
- (void)handleProfil {
    CEProfilViewController *profil = [CEProfilViewController new];
	profil.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:profil animated:YES];
}

- (void)handleSearch:(UITapGestureRecognizer *)gesture {
    UIImageView *s = (UIImageView *)[gesture view];
    if (s.tag == 0) {
        [self unfoldSearchView];
    } else {
        [self foldSearchView];
    }
}

- (void)unfoldSearchView {
    self.searchView.tag = 1;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView addSearchView];
        [self.view layoutIfNeeded];
    }];
}

- (void)foldSearchView {
    [self.contentView.searchView endEditing:YES];
    self.searchView.tag = 0;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView removeSearchView];
        [self.view layoutIfNeeded];
    }];
}

- (void)handleDistanceSlide:(UISlider *)slider {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    _contentView.searchView.distanceMax.text = [NSString stringWithFormat:@"%@km",[formatter stringFromNumber:@(slider.value)]];
}

- (void)handleAgeSlide:(UISlider *)slider {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.roundingIncrement = [NSNumber numberWithInteger:1];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    _contentView.searchView.ageMedium.text = [NSString stringWithFormat:@"%@",[formatter stringFromNumber:@(slider.value)]];
    
}

- (void)handleDateTaped:(UITextField *)textField {
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    textField.inputView = datePicker;
}

- (void)datePickerValueChanged:(UIDatePicker *)datePicker {
    NSString *str = [NSDateFormatter localizedStringFromDate:[datePicker date] dateStyle:kCFDateFormatterShortStyle timeStyle:kCFDateFormatterShortStyle];
    _contentView.searchView.date.text = str;
}
@end
