//
//  CEUtils.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSON(X) [NSJSONSerialization JSONObjectWithData:X options:NSJSONReadingMutableContainers error:nil];
@interface CEUtils : NSObject

// check for empty nil or null
bool isEmpty(id thing);

// ez alerts
+ (void)alert:(NSString *)msg withTitle:(NSString *)title;

@end
