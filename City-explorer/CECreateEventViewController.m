//
//  CECreateEventViewController.m
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CECreateEventViewController.h"
#import "CEProfilViewController.h"
#import "UIImage+deviceSpecificMedia.h"

@interface CECreateEventViewController ()

@end

@implementation CECreateEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addProfilButton];
	[self.contentView.ageSlider addTarget:self action:@selector(handleAgeSlide:) forControlEvents:UIControlEventValueChanged];
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapView:)];
	[self.contentView addGestureRecognizer:tap];
	self.contentView.date.textView.delegate = self;
	UITapGestureRecognizer *addDescription = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAddDescription:)];
	[self.contentView.descView addGestureRecognizer:addDescription];
	
	[self handleSexImages];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
	self.navigationController.navigationBar.translucent = YES;
	self.navigationController.navigationBar.topItem.title = @"City Explorer";
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithWhite:0.9 alpha:1]];
	UIImage *bg = [UIImage imageForDeviceWithName:@"create_bg-667"];
	[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:bg]];
	[self.navigationController.navigationBar setTitleTextAttributes:
	 [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:21],
	  NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
}

- (void)addProfilButton {
    UIImageView *profil = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sample_theo"]];
    profil.frame = CGRectMake(20, 0, 40, 40);
    profil.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfil)];
    [profil addGestureRecognizer:tap];
    profil.tag = 0;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:profil];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (void)loadView {
    _contentView = [CECreateEventView new];
    self.view = _contentView;
}

- (void)handleProfil {
    CEProfilViewController *profil = [CEProfilViewController new];
	profil.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:profil animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    [self handleDateTaped:textField];
}

- (void)handleAgeSlide:(UISlider *)slider {
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	formatter.roundingIncrement = [NSNumber numberWithInteger:1];
	formatter.numberStyle = NSNumberFormatterDecimalStyle;
	_contentView.ageDisplay.text = [NSString stringWithFormat:@"%@",[formatter stringFromNumber:@(slider.value)]];
}

- (void)handleTapView:(UITapGestureRecognizer *)tapGesture {
	[self.view endEditing:YES];
}

- (void)handleDateTaped:(UITextField *)textField {
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    textField.inputView = datePicker;
}

- (void)datePickerValueChanged:(UIDatePicker *)datePicker {
    NSString *str = [NSDateFormatter localizedStringFromDate:[datePicker date] dateStyle:kCFDateFormatterShortStyle timeStyle:kCFDateFormatterShortStyle];
	_contentView.date.textView.text = str;
}

- (void)handleAddDescription:(UITapGestureRecognizer *)gesture {
	UIViewController *ui = [UIViewController new];
	ui.view.backgroundColor = [UIColor whiteColor];
	[self.navigationController pushViewController:ui animated:YES];
}

- (void)handleSexImages {
	UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageTap:)];
	UITapGestureRecognizer *imageTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageTap:)];
	UITapGestureRecognizer *imageTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageTap:)];
	self.contentView.sexBoth.userInteractionEnabled = YES;
	self.contentView.sexMale.userInteractionEnabled = YES;
	self.contentView.sexFemale.userInteractionEnabled = YES;
	
	[self.contentView.sexBoth addGestureRecognizer:imageTap];
	[self.contentView.sexMale addGestureRecognizer:imageTap2];
	[self.contentView.sexFemale addGestureRecognizer:imageTap3];
	
	[self addObserver:self.contentView.sexBoth forKeyPath:@"tag" options:0 context:nil];
	
	
	self.contentView.sexBoth.tag = 1;
	self.contentView.sexMale.tag = 0;
	self.contentView.sexFemale.tag = 0;
	
	[self refreshImageLayout];
}

- (void)applyImageForTag:(UIImageView *)object {
	if (object == self.contentView.sexBoth) {
		if (self.contentView.sexBoth.tag == 1) {
			self.contentView.sexBoth.image = [UIImage imageNamed:@"sex_both_icon_selected"];
		} else {
			self.contentView.sexBoth.image = [UIImage imageNamed:@"sex_both_icon"];
		}
	}
	else if (object == self.contentView.sexMale) {
		if (self.contentView.sexMale.tag == 1) {
			self.contentView.sexMale.image = [UIImage imageNamed:@"sex_male_icon_selected"];
		} else {
			self.contentView.sexMale.image = [UIImage imageNamed:@"sex_male_icon"];
		}
	}
	else if (object == self.contentView.sexFemale) {
		if (self.contentView.sexFemale.tag == 1) {
			self.contentView.sexFemale.image = [UIImage imageNamed:@"sex_female_icon_selected"];
		} else {
			self.contentView.sexFemale.image = [UIImage imageNamed:@"sex_female_icon"];
		}
	}
}

- (void)refreshImageLayout {
	[self applyImageForTag:self.contentView.sexBoth];
	[self applyImageForTag:self.contentView.sexMale];
	[self applyImageForTag:self.contentView.sexFemale];
}

- (void)handleImageTap:(UITapGestureRecognizer *)gesture {
	UIImageView *taped = (UIImageView *)[gesture view];
	if (taped.tag == 1) {
		return;
	} else {
		if (taped == self.contentView.sexBoth) {
			self.contentView.sexBoth.tag = 1;
			self.contentView.sexMale.tag = 0;
			self.contentView.sexFemale.tag = 0;
		}
		else if (taped == self.contentView.sexMale) {
			self.contentView.sexBoth.tag = 0;
			self.contentView.sexMale.tag = 1;
			self.contentView.sexFemale.tag = 0;
			
		}
		else if (taped == self.contentView.sexFemale) {
			self.contentView.sexBoth.tag = 0;
			self.contentView.sexMale.tag = 0;
			self.contentView.sexFemale.tag = 1;
		}
		[self refreshImageLayout];
	}
}

@end
