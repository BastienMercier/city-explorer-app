//
//  CEEnums.h
//  City-explorer
//
//  Created by Administrateur on 18/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	kCEDirectionLeft,
	kCEDirectionRight
} kCEDirection;

@interface CEEnums : NSObject

@end
