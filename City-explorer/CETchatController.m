//
//  CETchatController.m
//  City-explorer
//
//  Created by Administrateur on 21/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CETchatController.h"
#import "CETchatTableViewCell.h"

#define BOUNDS [[UIScreen mainScreen] bounds]

@interface CETchatController ()
{
	CETchatTableViewCell *tchatCell;
}
@end

@implementation CETchatController

- (instancetype)init
{
	self = [super init];
	if (self) {
		_messages = @[@"coucou",@"salut ça va ?",@"bien et toi ? on se voit à quelle heure pour la fête ?"];
		_sizes = [NSMutableArray arrayWithCapacity:_messages.count];
		for (int i = 0; i < _messages.count+1; i++) {
			[_sizes addObject:@(45)];
		}
	}
	return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section; {
	return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath; {
	CETchatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell_tchat"];
	if (!cell) {
		cell = [[CETchatTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell_tchat"];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	[cell setMessage:_messages[indexPath.row] withMark:indexPath.row%2 size:[_sizes[indexPath.row] floatValue]];
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	if (indexPath.row == _messages.count) {
		if (tchatCell) {
			NSString *str = tchatCell.textInput.text;
			NSLog(@"%@",str);
			NSDictionary *d = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16.0] };
			CGRect r = [str boundingRectWithSize:CGSizeMake(BOUNDS.size.width*0.7, CGFLOAT_MAX)
										 options:NSStringDrawingUsesLineFragmentOrigin
									  attributes:d context:nil];
			_sizes[indexPath.row] = @(r.size.width+20);
			tchatCell = nil;
			return r.size.height+20;
		}
		return 45;
	}
	NSString *str = _messages[indexPath.row];
	NSDictionary *d = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:16.0] };
	CGRect r = [str boundingRectWithSize:CGSizeMake(BOUNDS.size.width*0.7, CGFLOAT_MAX)
					  options:NSStringDrawingUsesLineFragmentOrigin
				   attributes:d context:nil];
	_sizes[indexPath.row] = @(r.size.width+20);
	return r.size.height+20;
}
@end
