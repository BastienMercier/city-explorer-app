//
//  CEMyEventsViewController.h
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEMyEventsView.h"
#import "CEMyEventDataSource.h"
#import "CEUpdatedDataSource.h"

@interface CEMyEventsViewController : UIViewController<CEUpdatedDataSource, UITableViewDelegate>

@property (strong, nonatomic) CEMyEventsView        *contentView;
@property (strong, nonatomic) CEMyEventDataSource   *eventsDataSource;

@end
