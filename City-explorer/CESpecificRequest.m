//
//  CESpecificRequest.m
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import "CESpecificRequest.h"

@implementation CESpecificRequest

+ (CERequest *)specificRequestForMeWithBlock:(api_request_handler_t)block;
{
	CERequest *request = [CERequest new];
	
	request.endPoint = @"http://mdev.mobelive.com/api/friends/request/sent?apikey=41414cf1fd344325c8d1c0f9388ebe6f";
	request.method = @"GET";
	request.block = block;
	
	return request;
}

@end
