//
//  CESearchView.h
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CESearchView : UIView

// name
@property (strong, nonatomic) UIView        *eventNameWraper;
@property (strong, nonatomic) UITextField   *eventName;

// date
@property (strong, nonatomic) UIView        *dateWraper;
@property (strong, nonatomic) UIImageView   *callendarIcon;
@property (strong, nonatomic) UITextField   *date;

// distance
@property (strong, nonatomic) UIImageView   *locationIcon;
@property (strong, nonatomic) UILabel       *distanceLabel;
@property (strong, nonatomic) UILabel       *distanceMax;
@property (strong, nonatomic) UISlider      *distanceSlider;

// age
@property (strong, nonatomic) UIImageView   *ageIcon;
@property (strong, nonatomic) UILabel       *age;
@property (strong, nonatomic) UILabel       *ageMedium;
@property (strong, nonatomic) UISlider      *ageSlider;

@end
