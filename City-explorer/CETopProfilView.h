//
//  CETopProfilView.h
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEUser.h"

@interface CETopProfilView : UIView

@property (strong, nonatomic) UIView *events;
@property (strong, nonatomic) UIView *follower;
@property (strong, nonatomic) UIImageView *picture;
@property (strong, nonatomic) UILabel *userName;
@property (strong, nonatomic) UIButton *followingButton;

@property (strong, nonatomic) UILabel *followLabel;
@property (strong, nonatomic) UILabel *followCount;

@property (strong, nonatomic) UILabel *eventCount;
@property (strong, nonatomic) UILabel *eventLabel;

- (void)setData:(CEUser *)currentUser;

@end
