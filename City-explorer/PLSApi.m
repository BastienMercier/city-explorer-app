//
//  PLSApi.m
//  AsyncConnexion
//
//  Created by Administrateur on 10/10/13.
//  Copyright (c) 2013 Administrateur. All rights reserved.
//

#import "PLSApi.h"
#import "PLSAsyncRequest.h"
#import "CERequest.h"

static PLSApi* theApi;
#define API_ADDR @"www.city-explorer.com:3000"

@interface PLSApi()
{
	NSURL *_url;
}

@end

@implementation PLSApi

											// ------------------------------ //
											// --- Initialisation methods --- //
											// ------------------------------ //

+ (void) initialize
{
	if (self == [PLSApi class])
	{
    	NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024
    														 diskCapacity:20 * 1024 * 1024
    															 diskPath:nil];
    	[NSURLCache setSharedURLCache:URLCache];
		theApi = [[PLSApi alloc] init];
	}
}

+ (PLSApi*) api
{
	return theApi;
}

- (id) init
{
	if( !(self = [super init]) )
	{
		return nil;
	}
	NSString *stringURL = [NSString stringWithFormat:@"http://%@/",API_ADDR];
	_url = [NSURL URLWithString:stringURL];
	return self;
}

- (void)requestWithRequest:(CERequest *)request callback:(api_request_handler_t)callback; {
	[self request:request.endPoint
	   withMethod:request.method
	   withHeader:request.header
		 withBody:request.body
	 withCallback:callback];
}

- (void) request:(NSString *)endPoint withMethod:(NSString *)restMethod withHeader:(NSDictionary *)header withBody:(NSDictionary *)params withCallback:(api_request_handler_t)block
{
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
	[request setURL:[NSURL URLWithString:endPoint relativeToURL:_url]];
	[request setHTTPMethod:restMethod];
	if ([restMethod isEqualToString:@"PUT"] || [restMethod isEqualToString:@"DELETE"]) {
		[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	}
	if(header)
	{
		NSString *headerField = [header objectForKey:@"headerField"];
		NSString *value = [header objectForKey:@"value"];
		[request setValue:value forHTTPHeaderField:headerField];
	}
	
	if(params)
	{
		NSString* bodyparams = [self parseAndFormatParams:params];
		NSData *myRequestData = [NSData dataWithBytes:[bodyparams UTF8String] length:[bodyparams length]];
		[request setHTTPBody:myRequestData];
	}
	
	request.timeoutInterval = 30;
	PLSAsyncRequest *asyncRequest = [PLSAsyncRequest requestWithBlock:block];
	NSURLConnection* connect = [NSURLConnection connectionWithRequest:request delegate:asyncRequest];
	[connect start];
}

- (NSString*) parseAndFormatParams:(NSDictionary*)params{
	NSMutableArray *pairs = [[NSMutableArray alloc] initWithCapacity:0];
	for (NSString *key in params) {
		[pairs addObject:[NSString stringWithFormat:@"%@=%@", key, params[key]]];
	}
	NSString *requestParams = [pairs componentsJoinedByString:@"&"];
	return requestParams;
}

- (void) downloadDataAtURL:(NSURL*)url withBlock:(api_request_handler_t)block
{
	NSURLRequest* downloadRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
	PLSAsyncRequest* asyncRequest = [PLSAsyncRequest requestWithBlock:block];
	NSURLConnection* connexion = [[NSURLConnection alloc] initWithRequest:downloadRequest delegate:asyncRequest startImmediately:NO];
	[connexion scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
	[connexion start];
}

@end
