//
//  UIColor+CustomColor.h
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *)customBlueColor;
+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue A:(NSUInteger)alpha;
+ (UIColor *)tabBarColorIndicator;
@end
