//
//  FeedCell.h
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEEvent.h"

@interface CEFeedCell : UITableViewCell

@property (strong, nonatomic) CEEvent       *event;

@property (strong, nonatomic) UIImageView   *calendarIcon;
@property (strong, nonatomic) UILabel       *date;
@property (strong, nonatomic) UIImageView   *locationIcon;
@property (strong, nonatomic) UILabel       *distance;
@property (strong, nonatomic) UILabel       *city;
@property (strong, nonatomic) UILabel       *owner;
@property (strong, nonatomic) UILabel       *title;
@property (strong, nonatomic) UILabel       *eventDescription;
@property (strong, nonatomic) NSArray       *stars;

@end
