//
//  CEUpdatedDataSource.h
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CEUpdatedDataSource <NSObject>

@required
- (void)didUpdateDataSource;

@optional
- (void)willUpdateDataSource;

@end
