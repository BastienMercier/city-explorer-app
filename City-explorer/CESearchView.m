//
//  CESearchView.m
//  City-explorer
//
//  Created by theo damaville on 07/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CESearchView.h"
#import "UIFont+AppFont.h"

#define padding_feed 6.0f

@interface CESearchView () {
    BOOL _didUpdateConstraints;
}
@property (strong, nonatomic) NSLayoutConstraint *widthConstraintLocationIcon;
@property (strong, nonatomic) NSLayoutConstraint *heightConstraintLocationIcon;
@property (strong, nonatomic) NSLayoutConstraint *widthConstraintDateIcon;
@property (strong, nonatomic) NSLayoutConstraint *heightConstraintDateIcon;
@end

@implementation CESearchView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:1. alpha:0.5];
        self.layer.masksToBounds = YES;
        [self addSubview:self.eventNameWraper];
        [self addSubview:self.eventName];
        [self addSubview:self.dateWraper];
        [self addSubview:self.date];
        [self addSubview:self.callendarIcon];
        [self addSubview:self.locationIcon];
        [self addSubview:self.distanceLabel];
        [self addSubview:self.distanceMax];
        [self addSubview:self.distanceSlider];
        [self addSubview:self.ageIcon];
        [self addSubview:self.age];
        [self addSubview:self.ageMedium];
        [self addSubview:self.ageSlider];
    }
    return self;
}

- (UIView *)eventNameWraper {
    if (!_eventNameWraper) {
        _eventNameWraper = [UIView new];
        _eventNameWraper.translatesAutoresizingMaskIntoConstraints = NO;
        _eventNameWraper.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    }
    return _eventNameWraper;
}

- (UITextField *)eventName {
    if (!_eventName) {
        _eventName = [UITextField new];
        _eventName.translatesAutoresizingMaskIntoConstraints = NO;
        _eventName.backgroundColor = [UIColor clearColor];
		_eventName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nom de la soirée" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
		_eventName.textColor = [UIColor whiteColor];
    }
    return _eventName;
}

- (UIView *)dateWraper {
    if (!_dateWraper) {
        _dateWraper = [UIView new];
        _dateWraper.translatesAutoresizingMaskIntoConstraints = NO;
        _dateWraper.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    }
    return _dateWraper;
}

- (UIImageView *)callendarIcon {
    if (!_callendarIcon) {
        _callendarIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_calendar"]];
        _callendarIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _callendarIcon;
}

- (UITextField *)date {
    if (!_date) {
        _date = [UITextField new];
		_date.backgroundColor = [UIColor clearColor];
		_date.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
		_date.textColor = [UIColor whiteColor];
        _date.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _date;
}

- (UIImageView *)locationIcon {
    if (!_locationIcon) {
        _locationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon_big"]];
        _locationIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _locationIcon;
}

- (UILabel *)distanceMax {
    if (!_distanceMax) {
        _distanceMax = [UILabel new];
        _distanceMax.translatesAutoresizingMaskIntoConstraints = NO;
        _distanceMax.text = @"500m";
        _distanceMax.textColor = [UIColor whiteColor];
        _distanceMax.font = _distanceLabel.font;
    }
    return _distanceMax;
}

- (UILabel *)distanceLabel {
    if (!_distanceLabel) {
        _distanceLabel = [UILabel new];
        _distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _distanceLabel.text = @"Distance";
        _distanceLabel.font = [UIFont appFontLightOfSize:16];
        _distanceLabel.textColor = [UIColor whiteColor];
    }
    return _distanceLabel;
}

- (UISlider *)distanceSlider {
    if (!_distanceSlider) {
        _distanceSlider = [UISlider new];
        _distanceSlider.translatesAutoresizingMaskIntoConstraints = NO;
        _distanceSlider.minimumValue = 0.5;
        _distanceSlider.maximumValue = 10;
    }
    return _distanceSlider;
}

- (UIImageView *)ageIcon {
    if (!_ageIcon) {
        _ageIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gateau_icon"]];
        _ageIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _ageIcon;
}

- (UILabel *)age {
    if (! _age) {
        _age = [UILabel new];
        _age.text = @"Age";
        _age.font = _distanceLabel.font;
        _age.textColor = [UIColor whiteColor];
        _age.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _age;
}

- (UILabel *)ageMedium {
    if (!_ageMedium) {
        _ageMedium = [UILabel new];
        _ageMedium.translatesAutoresizingMaskIntoConstraints = NO;
        _ageMedium.textColor = [UIColor whiteColor];
        _ageMedium.text = @"20ans";
        _ageMedium.font = _distanceLabel.font;
    }
    return _ageMedium;
}

- (UISlider *)ageSlider {
    if (!_ageSlider) {
        _ageSlider = [UISlider new];
        _ageSlider.translatesAutoresizingMaskIntoConstraints = NO;
        _ageSlider.minimumValue = 15;
        _ageSlider.maximumValue = 55;
    }
    return _ageSlider;
}

- (void)setConstraintForEventNameWraper {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventNameWraper
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventNameWraper
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:0.95 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventNameWraper
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0.20 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventNameWraper
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0.0]];
}

- (void)setConstraintForEventName {
    CGFloat offset = 5;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventNameWraper
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:-offset]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventNameWraper
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventNameWraper
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventNameWraper
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:offset]];
}

- (void)setConstraintForDateWraper {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_dateWraper
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_eventNameWraper
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_dateWraper
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.95 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_dateWraper
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.20 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_dateWraper
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0.0]];
}

- (void)setConstraintForCalendarIcon {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_callendarIcon
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_dateWraper
                                                    attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_callendarIcon
                                                    attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_dateWraper
                                                    attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0.0]];
    
    _widthConstraintDateIcon = [NSLayoutConstraint constraintWithItem:_callendarIcon
                                                            attribute:NSLayoutAttributeWidth
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:_dateWraper
                                                            attribute:NSLayoutAttributeWidth
                                                           multiplier:0.0 constant:20.0];
    [self addConstraint:_widthConstraintDateIcon];
    _heightConstraintDateIcon = [NSLayoutConstraint constraintWithItem:_callendarIcon
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_dateWraper
                                                    attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:20.0];
    [self addConstraint:_heightConstraintDateIcon];
}

- (void)setConstraintForDate {
    CGFloat offset = 5;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_dateWraper
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_dateWraper
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_dateWraper
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:24+offset]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_dateWraper
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:offset]];
}

- (void)setConstraintForLocationIcon {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:padding_feed+3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_dateWraper
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:padding_feed*2]];
    
    _heightConstraintLocationIcon = [NSLayoutConstraint constraintWithItem:_locationIcon
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeHeight
                                                                multiplier:0.0 constant:25];
    [self addConstraint:_heightConstraintLocationIcon];
    _widthConstraintLocationIcon = [NSLayoutConstraint constraintWithItem:_locationIcon
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:_dateWraper
                                 attribute:NSLayoutAttributeHeight
                                multiplier:0.0 constant:18];
    [self addConstraint:_widthConstraintLocationIcon];
}

- (void)setConstraintForDistanceLabel {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceLabel
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_locationIcon
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceLabel
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_locationIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForDistanceMax {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceMax
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:-padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceLabel
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_distanceMax
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForDistanceSlider {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceSlider
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_distanceLabel
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceSlider
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_distanceMax
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:-padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceSlider
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_distanceMax
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_distanceSlider
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_distanceMax
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:-padding_feed]];
}

- (void)setConstraintForAgeIcon {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                    attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:padding_feed+3]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageIcon
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_locationIcon
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:padding_feed*2.5]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeHeight
                                                                multiplier:0.0 constant:25]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_locationIcon
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self
                                 attribute:NSLayoutAttributeHeight
                                multiplier:0.0 constant:18]];
}

- (void)setConstraintForAge {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageIcon
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForAgeMedium {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageMedium
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:-padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageMedium
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_age
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForAgeSlider {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_age
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageMedium
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0 constant:-padding_feed]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageMedium
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_ageSlider
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_ageMedium
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:-padding_feed]];
}

- (void)updateConstraints {
    if (! _didUpdateConstraints) {
        [self setConstraintForEventNameWraper];
        [self setConstraintForEventName];
        [self setConstraintForDateWraper];
        [self setConstraintForCalendarIcon];
        [self setConstraintForDate];
        [self setConstraintForLocationIcon];
        [self setConstraintForDistanceLabel];
        [self setConstraintForDistanceMax];
        [self setConstraintForDistanceSlider];
        [self setConstraintForAgeIcon];
        [self setConstraintForAge];
        [self setConstraintForAgeMedium];
        [self setConstraintForAgeSlider];
        _didUpdateConstraints = YES;
    }
    [super updateConstraints];
}
@end
