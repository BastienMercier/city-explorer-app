//
//  CEEventDetailViewController.h
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEMasterEventDetailView.h"

@interface CEEventDetailViewController : UIViewController<CEDetailMenuDelegate, UITextViewDelegate>

@property (strong, nonatomic) CEMasterEventDetailView *contentView;

@end
