//
//  CERequest.m
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import "CERequest.h"
#import "PLSApi.h"
#import "CECurrentUser.h"

@interface CERequest(){
	api_request_handler_t handler;
}
@end

@implementation CERequest

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.apikey = [CECurrentUser sharedUser].token;
    }
    return self;
}

- (void)setBlock:(api_request_handler_t)block
{
	handler = block;
}

- (api_request_handler_t)block {
	return handler;
}

- (void)resume {
	[[PLSApi api] requestWithRequest:self callback:^(NSData *data, NSError *error) {
		handler(data,error);
	}];
}

@end
