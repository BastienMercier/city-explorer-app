//
//  CEMyEventsView.m
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEMyEventsView.h"
#import "UIColor+CustomColor.h"

@interface CEMyEventsView () {
    BOOL _didUpdatedConstraints;
}

@end
@implementation CEMyEventsView

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"events_bg"]];
        [self addSubview:self.indicator];
        [self addSubview:self.tableEvents];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (UIView *)indicator {
    if (! _indicator) {
        _indicator = [UIView new];
        _indicator.backgroundColor = [UIColor tabBarColorIndicator];
        _indicator.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _indicator;
}

- (UITableView *)tableEvents {
    if (! _tableEvents) {
        _tableEvents = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableEvents.backgroundColor = [UIColor clearColor];
        _tableEvents.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _tableEvents;
}

- (void)setConstraintForTableEvents {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableEvents
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:65]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableEvents
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableEvents
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableEvents
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_indicator
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:0]];
}

- (void)setConstraintForIndicator {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:-49.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:3.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.33 constant:0.0]];
}

- (void)updateConstraints {
    if (! _didUpdatedConstraints) {
        [self setConstraintForIndicator];
        [self setConstraintForTableEvents];
        _didUpdatedConstraints = YES;
    }
    [super updateConstraints];
}

@end
