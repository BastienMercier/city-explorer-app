//
//  CETchatTableViewCell.h
//  tablechat
//
//  Created by Administrateur on 20/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEInvalidateDelegate.h"

@interface CETchatTableViewCell : UITableViewCell

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) UIView *bubble;
@property (strong, nonatomic) UILabel *msg;
@property (nonatomic) NSInteger mark;
@property (nonatomic) CGFloat labelWidth;
@property (strong, nonatomic) UITextView *textInput;

@property (nonatomic) CGFloat lines;

- (void)setMessage:(NSString *)message withMark:(NSInteger)mark size:(CGFloat)size;

@property (strong, nonatomic) id<CEInvalidateDelegate> delegate;
@end
