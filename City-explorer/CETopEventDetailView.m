//
//  CETopEventDetailView.m
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CETopEventDetailView.h"
#import "UIColor+CustomColor.h"
#import "UIFont+AppFont.h"


@interface  CETopEventDetailView(){
	BOOL _didUpdateConstraints;
}
@end

@implementation CETopEventDetailView

- (instancetype)init {
	return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor clearColor];
		[self addSubview:self.userPicture];
		[self addSubview:self.userName];
		[self addSubview:self.eventName];
		[self setNeedsUpdateConstraints];
	}
	return self;
}

- (UIImageView *)userPicture {
	if (! _userPicture) {
		_userPicture = [UIImageView new];
		_userPicture.translatesAutoresizingMaskIntoConstraints = NO;
		_userPicture.image = [UIImage imageNamed:@"sample_theo"];
		_userPicture.layer.borderColor = [UIColor tabBarColorIndicator].CGColor;
		_userPicture.layer.borderWidth = 1;
	}
	return _userPicture;
}

- (UILabel *)userName {
	if (! _userName) {
		_userName = [UILabel new];
		_userName.translatesAutoresizingMaskIntoConstraints = NO;
		_userName.font = [UIFont appFontOfSize:20];
		_userName.text = @"Bastien Mercier";
		_userName.textColor = [UIColor whiteColor];
	}
	return _userName;
}

- (UILabel *)eventName {
	if (! _eventName) {
		_eventName = [UILabel new];
		_eventName.translatesAutoresizingMaskIntoConstraints = NO;
		_eventName.textColor = _userName.textColor;
		_eventName.text = @"soirée petite radasse";
		_eventName.font = [UIFont appFontOfSize:22];
	}
	return _eventName;
}

- (void)setConstraintForPicture {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userPicture
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.6 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userPicture
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.6 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userPicture
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeLeft
													multiplier:1.0 constant:20]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userPicture
													 attribute:NSLayoutAttributeCenterY
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterY
													multiplier:1.0 constant:0]];
}

- (void)setConstraintForNickname {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userName
													 attribute:NSLayoutAttributeCenterY
													 relatedBy:NSLayoutRelationEqual
														toItem:_userPicture
													 attribute:NSLayoutAttributeCenterY
													multiplier:1.0 constant:-10]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_userName
													 attribute:NSLayoutAttributeRight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeRight
													multiplier:1.0 constant:-15]];
}

- (void)setConstraintForEventName {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
													 attribute:NSLayoutAttributeRight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeRight
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_eventName
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:_userName
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
}

- (void)layoutSubviews {
	[super layoutSubviews];
	_userPicture.layer.masksToBounds = YES;
	_userPicture.layer.cornerRadius = _userPicture.frame.size.height/2.0;
}

- (void)updateConstraints {
	if (! _didUpdateConstraints) {
		_didUpdateConstraints = YES;
		[self setConstraintForPicture];
		[self setConstraintForNickname];
		[self setConstraintForEventName];
	}
	[super updateConstraints];
}

@end
