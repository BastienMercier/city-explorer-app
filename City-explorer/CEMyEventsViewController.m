//
//  CEMyEventsViewController.m
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEMyEventsViewController.h"
#import "CEProfilViewController.h"
#import "CEEventDetailViewController.h"

@interface CEMyEventsViewController ()

@end

@implementation CEMyEventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addProfilButton];
    [self configureTableEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_contentView.tableEvents deselectRowAtIndexPath:[_contentView.tableEvents indexPathForSelectedRow] animated:YES];
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
	self.navigationController.navigationBar.translucent = YES;
	self.navigationController.navigationBar.topItem.title = @"City Explorer";
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithWhite:0.9 alpha:1]];
	[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"events_bg"]]];
	[self.navigationController.navigationBar setTitleTextAttributes:
	 [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:21],
	  NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
}

- (void)addProfilButton {
    UIImageView *profil = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sample_theo"]];
    profil.frame = CGRectMake(20, 0, 40, 40);
    profil.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfil)];
    [profil addGestureRecognizer:tap];
    profil.tag = 0;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:profil];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (void)handleProfil {
    CEProfilViewController *profil = [CEProfilViewController new];
	profil.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:profil animated:YES];
}

- (void)loadView {
    _contentView = [CEMyEventsView new];
    self.view = _contentView;
}

- (void)configureTableEvent {
    _eventsDataSource = [CEMyEventDataSource new];
    _eventsDataSource.delegate = self;
    _contentView.tableEvents.dataSource = _eventsDataSource;
    _contentView.tableEvents.delegate = self;
}

#pragma mark datasource events
- (void)didUpdateDataSource {
    [_contentView.tableEvents reloadData];
}

- (void)willUpdateDataSource {
    
}

#pragma mark tableEvent delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 160;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CEEventDetailViewController *detail = [CEEventDetailViewController new];
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
}

@end
