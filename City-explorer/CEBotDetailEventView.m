//
//  CEBotDetailEventView.m
//  City-explorer
//
//  Created by theo damaville on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEBotDetailEventView.h"
#import "UIColor+CustomColor.h"

@interface  CEBotDetailEventView(){
	BOOL _didUpdateConstraints;
}
@end

@implementation CEBotDetailEventView

- (instancetype)init {
	return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor colorWithR:20 G:20 B:20 A:1];
        [self addSubview:self.date];
        [self addSubview:self.hour];
        [self addSubview:self.city];
        [self addSubview:self.location];
        [self addSubview:self.age];
        [self addSubview:self.subscribe];
		[self setNeedsUpdateConstraints];
	}
	return self;
}

- (CETextFieldWithIcon *)date {
    if (! _date) {
        _date = [CETextFieldWithIcon new];
        _date.translatesAutoresizingMaskIntoConstraints = NO;
        _date.iconView.image = [UIImage imageNamed:@"icon_calendar"];
        _date.textView.text = @"20 Novembre";
        _date.textView.textColor = [UIColor whiteColor];
    }
    return _date;
}
- (CETextFieldWithIcon *)hour {
    if (! _hour) {
        _hour = [CETextFieldWithIcon new];
        _hour.translatesAutoresizingMaskIntoConstraints = NO;
        _hour.iconView.image = [UIImage imageNamed:@"tab_horloge"];
        _hour.textView.text = @"00:00";
        _hour.textView.textColor = _date.textView.textColor;
    }
    return _hour;
}
- (CETextFieldWithIcon *)location {
    if (! _location) {
        _location = [[CETextFieldWithIcon alloc] initWithFrame:CGRectZero imageSize:CGSizeMake(0.5, 0.7)];
        _location.translatesAutoresizingMaskIntoConstraints = NO;
        _location.iconView.image = [UIImage imageNamed:@"geoloc 3"];
        _location.textView.text = @"500m";
        _location.textView.textColor = _date.textView.textColor;
        
    }
    return _location;
}
- (CETextFieldWithIcon *)city {
    if (! _city) {
        _city = [CETextFieldWithIcon new];
        _city.translatesAutoresizingMaskIntoConstraints = NO;
        _city.iconView.image = [UIImage imageNamed:@"city_icone"];
        _city.textView.text = @"le havre";
        _city.textView.textColor = _date.textView.textColor;
    }
    return _city;
}

- (CETextFieldWithIcon *)age {
    if (! _age) {
        _age = [CETextFieldWithIcon new];
        _age.translatesAutoresizingMaskIntoConstraints = NO;
        _age.iconView.image = [UIImage imageNamed:@"gateau_icon"];
        _age.textView.text = @"23 ans";
        _age.textView.textColor = _date.textView.textColor;
    }
    return _age;
}

- (UIButton *)subscribe {
    if (! _subscribe) {
        _subscribe = [UIButton buttonWithType:UIButtonTypeCustom];
        _subscribe.translatesAutoresizingMaskIntoConstraints = NO;
        [_subscribe setTitle:@"S'inscrire" forState:UIControlStateNormal];
        [_subscribe setTintColor:[UIColor whiteColor]];
        _subscribe.backgroundColor = [UIColor colorWithR:69 G:225 B:122 A:1];
    }
    return _subscribe;
}

- (void)setConstraints {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.7
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_date
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.1
                                                      constant:0]];
    
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_hour
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_date
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_hour
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_hour
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.7
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_hour
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.1
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_hour
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.7
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_city
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.1
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_location
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_city
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_location
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_location
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.7
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_location
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.1
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_location
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.7
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_age
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.1
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_subscribe
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:-15]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_subscribe
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_subscribe
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.6
                                                      constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_subscribe
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.15
                                                      constant:0]];
}

- (void)updateConstraints {
	if (! _didUpdateConstraints) {
		_didUpdateConstraints = YES;
        [self setConstraints];
	}
    [super updateConstraints];
}

- (UIColor *)bgColor {
    return [UIColor colorWithR:48 G:48 B:48 A:1];
}
@end
