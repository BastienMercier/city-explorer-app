//
//  CEUtils.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEUtils.h"
#import <UIKit/UIKit.h>

@implementation CEUtils

bool isEmpty(id thing)
{
    return thing == nil
	|| (thing == [NSNull null])
	|| ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
	|| ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

+ (void)alert:(NSString *)msg withTitle:(NSString *)title
{
   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
												   message:msg
												  delegate:nil
										 cancelButtonTitle:@"OK"
										 otherButtonTitles:nil];
	[alert show];
}
@end
