//
//  CETchatTableViewCell.m
//  tablechat
//
//  Created by Administrateur on 20/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import "CETchatTableViewCell.h"

@implementation CETchatTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		self.backgroundColor = [UIColor clearColor];
		self.contentView.backgroundColor = [UIColor clearColor];
	}
	return self;
}

- (void)setMessage:(NSString *)message withMark:(NSInteger)mark size:(CGFloat)size{
	_message = message;
	_mark = mark;
	_labelWidth = size;
	[self.contentView addSubview:self.bubble];
	[self.bubble addSubview:self.msg];
	[self setConstraints];
	[self setNeedsLayout];
}

- (UILabel *)msg {
	if (! _msg) {
		_msg = [UILabel new];
		_msg.text = _message;
		_msg.translatesAutoresizingMaskIntoConstraints = NO;
		_msg.lineBreakMode = NSLineBreakByWordWrapping;
		_msg.numberOfLines = 0;
		_msg.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
		if (_mark == 1) {
    		_msg.textColor = [UIColor whiteColor];
		} else {
    		_msg.textColor = [UIColor blackColor];
		}
	}
	return _msg;
}

- (UIView *)bubble {
	if (! _bubble) {
		_bubble = [UIView new];
		_bubble.translatesAutoresizingMaskIntoConstraints = NO;
		if (_mark == 1) {
			_bubble.backgroundColor = [UIColor colorWithRed:31/255. green:218/255. blue:60/255. alpha:1];
		} else {
			_bubble.backgroundColor = [UIColor colorWithRed:230/255. green:230/255. blue:230/255. alpha:1];
		}
	}
	return _bubble;
}

- (void)setConstraints {
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_bubble
																attribute:NSLayoutAttributeCenterY
																relatedBy:NSLayoutRelationEqual
																   toItem:self.contentView
																attribute:NSLayoutAttributeCenterY
																multiplier:1.0 constant:0]];
	
	CGFloat margin = 5;
	NSLayoutAttribute attr = NSLayoutAttributeLeft;
	if (_mark == 1) {
		attr = NSLayoutAttributeRight;
    	margin = -5;
	}
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_bubble
																attribute:attr
																relatedBy:NSLayoutRelationEqual
																   toItem:self.contentView
																attribute:attr
																multiplier:1.0 constant:margin]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_bubble
																attribute:NSLayoutAttributeHeight
																relatedBy:NSLayoutRelationEqual
																   toItem:self.contentView
																attribute:NSLayoutAttributeHeight
																multiplier:1.0 constant:-10]];
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_bubble
																attribute:NSLayoutAttributeWidth
																relatedBy:NSLayoutRelationEqual
																   toItem:self.contentView
																attribute:NSLayoutAttributeWidth
																multiplier:0 constant:_labelWidth]];
	
	
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_msg
																attribute:NSLayoutAttributeCenterX
																relatedBy:NSLayoutRelationEqual
																   toItem:self.bubble
																attribute:NSLayoutAttributeCenterX
																multiplier:1.0 constant:0]];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_msg
																attribute:NSLayoutAttributeWidth
																relatedBy:NSLayoutRelationEqual
																   toItem:self.bubble
																attribute:NSLayoutAttributeWidth
																multiplier:1 constant:-10]];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_msg
																attribute:NSLayoutAttributeCenterY
																relatedBy:NSLayoutRelationEqual
																   toItem:self.bubble
																attribute:NSLayoutAttributeCenterY
																multiplier:1.0 constant:0]];
}

/*- (UITextView *)textInput {
	if (! _textInput) {
		_textInput = [UITextView new];
		_textInput.backgroundColor = [UIColor redColor];
		_textInput.translatesAutoresizingMaskIntoConstraints = NO;
		_textInput.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
		_textInput.delegate = self;
	}
	return _textInput;
}*/

/*- (void)textViewDidChange:(UITextView *)textView {
	NSString *str = textView.text;
	NSDictionary *d = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16.0] };
	CGRect r = [str boundingRectWithSize:CGSizeMake(self.contentView.bounds.size.width*0.7, CGFLOAT_MAX)
								 options:NSStringDrawingUsesLineFragmentOrigin
							  attributes:d context:nil];
	if (_lines != r.size.height) {
		_lines = r.size.height;
		[self invalidateMe];
	}
}*/

/*- (void)invalidateMe {
	[_delegate shouldInvalidate:self];
}*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	if (!selected || !_delegate) {
		return;
	}/*
	[self.bubble addSubview:self.textInput];
	[self.textInput becomeFirstResponder];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_textInput
															attribute:NSLayoutAttributeCenterX
															relatedBy:NSLayoutRelationEqual
															   toItem:_bubble
															attribute:NSLayoutAttributeCenterX
														   multiplier:1.0 constant:0]];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_textInput
															attribute:NSLayoutAttributeCenterY
															relatedBy:NSLayoutRelationEqual
															   toItem:_bubble
															attribute:NSLayoutAttributeCenterY
														   multiplier:1.0 constant:0]];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_textInput
															attribute:NSLayoutAttributeWidth
															relatedBy:NSLayoutRelationEqual
															   toItem:_bubble
															attribute:NSLayoutAttributeWidth
														   multiplier:1.0 constant:0]];
	[self.bubble addConstraint:[NSLayoutConstraint constraintWithItem:_textInput
															attribute:NSLayoutAttributeHeight
															relatedBy:NSLayoutRelationEqual
															   toItem:_bubble
															attribute:NSLayoutAttributeHeight
														   multiplier:1.0 constant:0]];
	  */
}

@end
