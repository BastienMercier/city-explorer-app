//
//  CETabBarController.h
//  City-explorer
//
//  Created by theo damaville on 04/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CETabBarController : NSObject

+ (UITabBarController *) createMeTabBarController;

@end
