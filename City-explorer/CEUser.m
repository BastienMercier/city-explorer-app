//
//  CEUser.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEUser.h"

@implementation CEUser

+ (CEUser *)userWithDictionary:(NSDictionary *)userInfos {
	CEUser *obj = [[CEUser alloc] initWithDictionary:userInfos];
	if (obj) {
		return obj;
	}
	return nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)user;
{
	self = [super init];
	if (self) {
		_provider = kCEProviderFacebook;
		_userId = user[@"_id"];
		_createdAt = user[@"created_at"];
		_firstname = user[@"firstname"];
		_lastname = user[@"lastname"];
		_picturePath = user[@"urlImage"];
		_followers = user[@"followers"];
		_following = user[@"followers"];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	if(!self) {
		return nil;
	}
	
	self.provider = [aDecoder decodeIntegerForKey:@"provider"];
	self.userDescription = [aDecoder decodeObjectForKey:@"userDescription"];
	self.followers = [aDecoder decodeObjectForKey:@"followers"];
	self.following = [aDecoder decodeObjectForKey:@"following"];
	self.userId = [aDecoder decodeObjectForKey:@"userId"];
	self.updatedAt = [aDecoder decodeObjectForKey:@"updatedAt"];
	self.createdAt = [aDecoder decodeObjectForKey:@"createdAt"];
	self.firstname = [aDecoder decodeObjectForKey:@"firstname"];
	self.lastname = [aDecoder decodeObjectForKey:@"lastname"];
	self.picturePath = [aDecoder decodeObjectForKey:@"picturePath"];
	self.picture = [aDecoder decodeObjectForKey:@"picture"];
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeInteger:self.provider forKey:@"provider"];
	[coder encodeObject:self.userDescription forKey:@"userDescription"];
	[coder encodeObject:self.following forKey:@"following"];
	[coder encodeObject:self.followers forKey:@"followers"];
	[coder encodeObject:self.userId forKey:@"userId"];
	[coder encodeObject:self.updatedAt forKey:@"updatedAt"];
	[coder encodeObject:self.createdAt forKey:@"createdAt"];
	[coder encodeObject:self.firstname forKey:@"firstname"];
	[coder encodeObject:self.lastname forKey:@"lastname"];
	[coder encodeObject:self.picture forKey:@"picture"];
	[coder encodeObject:self.picturePath forKey:@"picturePath"];
}

- (NSString *)description {
	return [NSString stringWithFormat:@"<%@> id:%@ name:%@",[self class], _userId, _firstname];
}

@end
