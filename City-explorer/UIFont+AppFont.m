//
//  UIFOnt.m
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "UIFont+AppFont.h"

@implementation UIFont (AppFont)

+ (UIFont *)appFontOfSize:(NSInteger)size {
    return [UIFont fontWithName:@"Avenir" size:size];
}

+ (UIFont *)appFontLightOfSize:(NSInteger)size ;
{
    return [UIFont fontWithName:@"Avenir-Light" size:size];
}
@end
