//
//  CEFeedView.m
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEFeedView.h"
#import "UIColor+CustomColor.h"

@interface CEFeedView() {
    BOOL _didUpdatedConstraints;
}

@end

@implementation CEFeedView

- (instancetype)init
{
	return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"feed_bg"]];
        [self addSubview:self.tableFeed];
        [self addSubview:self.searchView];
        [self addSubview:self.indicator];
        [self setNeedsUpdateConstraints];
	}
	return self;
}

- (UITableView *)tableFeed {
    if (!_tableFeed) {
        _tableFeed = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableFeed.backgroundColor = [UIColor clearColor];
        _tableFeed.translatesAutoresizingMaskIntoConstraints = NO;
        _tableFeed.contentInset = UIEdgeInsetsZero;
    }
    return _tableFeed;
}

- (void)addSearchView {
    _heightSearchViewConstraint.constant = 170;
}


- (void)searchViewOffset:(CGFloat)offset {
    _heightSearchViewConstraint.constant = _heightSearchViewConstraint.constant+offset;
}

- (void)removeSearchView {
    _heightSearchViewConstraint.constant = 0;
}

- (CESearchView *)searchView {
    if (!_searchView) {
        _searchView = [CESearchView new];
        _searchView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _searchView;
}

- (UIView *)indicator {
    if (! _indicator) {
        _indicator = [UIView new];
        _indicator.backgroundColor = [UIColor tabBarColorIndicator];
        _indicator.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _indicator;
}

- (void)addSearchViewConstraints {
    _heightSearchViewConstraint = [NSLayoutConstraint constraintWithItem:_searchView
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:0.0 constant:0.0];
    [self addConstraint:_heightSearchViewConstraint];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_searchView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0 constant:65.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_searchView
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_searchView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0.0]];
}

- (void)setConstraintsForTable {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableFeed
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableFeed
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_tableFeed
                                                    attribute:NSLayoutAttributeBottom
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:-52.0]];
    _tableTopConstraint = [NSLayoutConstraint constraintWithItem:_tableFeed
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_searchView
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1.0 constant:0.0];
    [self addConstraint:_tableTopConstraint];
}

- (void)setConstraintForIndicator {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0 constant:-49.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:0.0 constant:3.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_indicator
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:0.33 constant:0.0]];
}

-(void)updateConstraints {
    if (!_didUpdatedConstraints) {
        [self setConstraintsForTable];
        [self addSearchViewConstraints];
        [self setConstraintForIndicator];
        _didUpdatedConstraints = YES;
    }
    [super updateConstraints];
}

@end
