//
//  CEScrollerTuto.m
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEScrollerTuto.h"
#import "UIColor+CustomColor.h"
#import "UIFont+AppFont.h"

#define BOT_RATIO 0.2
@implementation CEScrollerTuto

- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"signup_bg"]];
        [self addSubview:self.botView];
        [self addSubview:self.scrollerImage];
        [self loadIphone];
        [self.botView addSubview:self.facebook];
        [self loadWarning];
        [self.botView addSubview:self.pageControl];
        [self loadScrollText];
    }
    return self;
}

- (void)loadIphone {
    CGFloat mw = self.frame.size.width/2;
    CGFloat mh = self.frame.size.height/2;
    UIImageView *iView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone"]];
    iView.frame = CGRectMake(mw-75, mh-150, 150, 300);
    [self addSubview:iView];
}

- (UIScrollView *)scrollerImage {
    if (!_scrollerImage) {
        CGFloat mw = self.frame.size.width/2;
        CGFloat mh = self.frame.size.height/2;
        _scrollerImage = [[UIScrollView alloc] initWithFrame:CGRectMake(mw-67, mh-(234/2), 134, 234)];
        _scrollerImage.contentSize = CGSizeMake(134*3,234);
        _scrollerImage.pagingEnabled = YES;
        _scrollerImage.backgroundColor = [UIColor redColor];
        _scrollerImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"test"]];
    }
    return _scrollerImage;
}

- (UIView *)botView {
    if (!_botView) {
        CGFloat bh = self.frame.size.height*BOT_RATIO;
        _botView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-bh, self.frame.size.width, bh)];
        _botView.backgroundColor = [UIColor whiteColor];
    }
    return _botView;
}

- (UIButton *)facebook {
    if (!_facebook) {
        _facebook = [UIButton buttonWithType:UIButtonTypeSystem];
        
        CGFloat sizeX = _botView.frame.size.width * 0.9;
        CGFloat sizeY = _botView.frame.size.height * 0.4;
        CGFloat posX  = _botView.frame.size.width/2.0 - sizeX/2.0;
        CGFloat posY  = _botView.frame.size.height/2.0 - sizeY/2.0;
        
        _facebook.frame = CGRectMake(posX, posY, sizeX, sizeY);
        [_facebook setTitle:@"Se connecter via facebook" forState:UIControlStateNormal];
        [_facebook setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _facebook.titleLabel.font = [UIFont appFontOfSize:18];
        _facebook.backgroundColor = [UIColor colorWithR:59 G:89 B:152 A:1];
    }
    return _facebook;
}

- (void)loadWarning {
    UILabel *warn = [UILabel new];
    warn.text = @"Nous ne publions rien sans votre authorisation";
    warn.frame = CGRectMake(0, 0, self.frame.size.width, 15);
    warn.font = [UIFont appFontOfSize:11];
    warn.textAlignment = NSTextAlignmentCenter;
    warn.center = CGPointMake(_botView.center.x, _facebook.frame.origin.y + _facebook.frame.size.height + 10);
    [_botView addSubview:warn];
}

- (SMPageControl *)pageControl;
{
    if (!_pageControl) {
    	_pageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(_botView.frame.size.width/2-30,_facebook.frame.origin.y-33,60,10)];
    	_pageControl.numberOfPages = 3;
    	_pageControl.currentPage = 0;
		_pageControl.pageIndicatorImage = [UIImage imageNamed:@"pageDot"];
		_pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"currentPageDot"];
		[_pageControl sizeToFit];
        //_pageControl.indicatorDiameter = 5;
    }
    return _pageControl;
}

- (void)loadScrollText {
    CGFloat screenSize = self.frame.size.width;
    CGFloat sizeY = self.frame.size.height - self.frame.size.height*BOT_RATIO;
    CGRect rect = CGRectMake(0, 0, self.frame.size.width, sizeY);
    _scrollerText = [[UIScrollView alloc] initWithFrame:rect];
    _scrollerText.backgroundColor = [UIColor clearColor];
    _scrollerText.contentSize = CGSizeMake(self.frame.size.width*3, sizeY);
    _scrollerText.pagingEnabled = YES;
    _scrollerText.showsHorizontalScrollIndicator = NO;
    _scrollerText.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollerText];
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, sizeY*0.1, screenSize, 30)];
    l.textColor = [UIColor whiteColor];
    l.text = @"Organisez une soirée";
    l.font = [UIFont appFontLightOfSize:28];
    l.textAlignment = NSTextAlignmentCenter;
    [_scrollerText addSubview:l];
    
    UILabel *l2 = [[UILabel alloc] initWithFrame:CGRectMake(screenSize, sizeY*0.1, screenSize, 30)];
    l2.textColor = [UIColor whiteColor];
    l2.textAlignment = NSTextAlignmentCenter;
    l2.font = l.font;
    l2.text = @"Ou créez en une";
    [_scrollerText addSubview:l2];
    
    UILabel *l3 = [[UILabel alloc] initWithFrame:CGRectMake(screenSize*2, sizeY*0.1, screenSize, 30)];
    l3.textColor = [UIColor whiteColor];
    l3.textAlignment = NSTextAlignmentCenter;
    l3.text = @"Lancez-vous !";
    l3.font = l.font;
    [_scrollerText addSubview:l3];
}
@end
