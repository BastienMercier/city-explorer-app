//
//  CESpecificRequest.h
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CERequest.h"

@interface CESpecificRequest : NSObject

+ (CERequest *)specificRequestForMeWithBlock:(api_request_handler_t)block;

@end
