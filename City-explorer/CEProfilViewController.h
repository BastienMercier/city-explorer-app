//
//  CEProfilViewController.h
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEProfilView.h"
#import "CEUpdatedDataSource.h"
#import "CEProfileDataSource.h"

@interface CEProfilViewController : UIViewController<CEUpdatedDataSource>

@property (strong, nonatomic) CEProfilView *contentView;
@property (strong, nonatomic) CEProfileDataSource *profilDataSource;

@end
