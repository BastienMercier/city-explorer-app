//
//  CEMyEventsView.h
//  City-explorer
//
//  Created by theo damaville on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEMyEventsView : UIView

@property (strong, nonatomic) UITableView   *tableEvents;
@property (strong, nonatomic) UIView        *indicator;

@end
