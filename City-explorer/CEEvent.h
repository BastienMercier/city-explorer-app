//
//  CEEvent.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEUser.h"
#import "CEUtils.h"

typedef enum kCEGender {
	kCEGenderMale = 0,
	kCEGenderFemale = 1,
	kCEGenderMaleAndFemale = 2
} kCEGender ;

@interface CEEvent : NSObject

@property (strong, nonatomic) CEUser   *owner;
@property (strong, nonatomic) NSString *eventId;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *adress;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *age;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *EventDescription;
@property (strong, nonatomic) NSArray  *participants;
@property (strong, nonatomic) NSDate   *updatedAt;
@property (strong, nonatomic) NSDate   *createdAt;

@property (nonatomic) NSInteger rank;
@property (nonatomic) BOOL active;

@end
