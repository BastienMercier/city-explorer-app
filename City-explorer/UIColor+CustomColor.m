//
//  UIColor+CustomColor.m
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColors)

+ (UIColor *)colorWithR:(NSUInteger)red G:(NSUInteger)green B:(NSUInteger)blue A:(NSUInteger)alpha {
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0f];
}

+ (UIColor *)tabBarColorIndicator
{
    return [UIColor colorWithR:7 G:231 B:247 A:1];
}

+ (UIColor *)customBlueColor {
    return [UIColor blueColor];
}

@end
