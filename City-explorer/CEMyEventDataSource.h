//
//  CEMyEventDataSource.h
//  City-explorer
//
//  Created by theo damaville on 16/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEUpdatedDataSource.h"
#import <UIKit/UIKit.h>

@interface CEMyEventDataSource : NSObject<UITableViewDataSource>

@property (strong, nonatomic) id<CEUpdatedDataSource> delegate;

@end
