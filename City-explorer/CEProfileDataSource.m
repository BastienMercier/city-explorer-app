//
//  CEProfileDataSource.m
//  City-explorer
//
//  Created by Administrateur on 03/12/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEProfileDataSource.h"
#import "CECacheManager.h"
#import "CEUserRequest.h"
#import "CECurrentUser.h"

@interface CEProfileDataSource () {
	CECacheManager *_cache;
}

@end

@implementation CEProfileDataSource

- (instancetype)init
{
	self = [super init];
	if (self) {
		_cache = [CECacheManager new];
		CECurrentUser *u = [_cache objectForKey:kCECurrentUser];
		_currentUser = u.currentUser;
	}
	return self;
}

- (void)setDelegate:(id<CEUpdatedDataSource>)delegate {
	_delegate = delegate;
	if (_currentUser) {
		[self noticeDelegate];
	}
	[self fetchProfilData];
}

- (void)fetchProfilData {
	[CEUserRequest getUserWithId:_currentUser.userId withBlock:^(NSData *data, NSError *error) {
		if (error) {
			[CEUtils alert:@"err_fetch_profil" withTitle:@"api_err"];
		} else {
			id json = JSON(data);
			NSLog(@"profil : %@",json);
			CEUser *user = [CEUser userWithDictionary:json];
			_currentUser = user;
			[CEUserRequest imageForUser:_currentUser.picturePath withBlock:^(UIImage *userImage) {
				_currentUser.picture = userImage;
				[self noticeDelegate];
			}];
		}
	}];
}

- (void)noticeDelegate {
	if ([_delegate respondsToSelector:@selector(didUpdateDataSource)]) {
		[_delegate didUpdateDataSource];
	}
}
@end
