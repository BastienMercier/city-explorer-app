//
//  AppDelegate.h
//  City-explorer
//
//  Created by theo damaville on 31/10/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CECacheManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) CECacheManager *cacheManager;


@end

