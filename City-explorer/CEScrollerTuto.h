//
//  CEScrollerTuto.h
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"

@interface CEScrollerTuto : UIView

@property (strong, nonatomic) UIScrollView *scrollerImage;
@property (strong, nonatomic) UIScrollView *scrollerText;
@property (strong, nonatomic) UIView *botView;
@property (strong, nonatomic) UIButton *facebook;
@property (strong, nonatomic) SMPageControl *pageControl;

@property (nonatomic) CGFloat lastOffset;

@end
