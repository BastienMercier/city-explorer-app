//
//  CEUserRequest.h
//  City-explorer
//
//  Created by theo damaville on 28/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CERequest.h"
#import <UIKit/UIKit.h>

@interface CEUserRequest : NSObject

+ (CERequest *)getAllUsersWithBlock:(api_request_handler_t)block;
+ (CERequest *)getUserWithId:(NSString *)uid withBlock:(api_request_handler_t)block;
+ (CERequest *)postCreateUser:(NSDictionary *)userInfos withBlock:(api_request_handler_t)block;
+ (CERequest *)postToggleFollowUserWithId:(NSString *)uid withBlock:(api_request_handler_t)block;
+ (void) imageForUser:(NSString *)strURL withBlock:(void (^) (UIImage *userImage))block;

@end
