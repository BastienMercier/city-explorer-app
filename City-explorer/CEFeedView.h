//
//  CEFeedView.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CESearchView.h"

@interface CEFeedView : UIView

@property (strong, nonatomic) UITableView *tableFeed;
@property (strong, nonatomic) id botLayout;
@property (strong, nonatomic) NSLayoutConstraint *tableTopConstraint;
@property (strong, nonatomic) NSLayoutConstraint *heightSearchViewConstraint;
@property (strong, nonatomic) CESearchView *searchView;
@property (strong, nonatomic) UIView *indicator;

- (void)addSearchView;
- (void)removeSearchView;
- (void)searchViewOffset:(CGFloat)offset;

@end
