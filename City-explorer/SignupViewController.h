//
//  SignupViewController.h
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CEScrollerTuto.h"

@interface SignupViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) CEScrollerTuto *content;

@end
