//
//  CEDetailEventMenu.m
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEDetailEventMenu.h"

@interface  CEDetailEventMenu(){
	BOOL _didUpdateConstraints;
}
@end

@implementation CEDetailEventMenu

- (instancetype)init {
	return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor clearColor];
		[self addSubview:self.descriptionView];
		[self addSubview:self.messageView];
		[self.descriptionView addSubview:self.descriptionLabel];
		[self.messageView addSubview:self.messageLabel];
		[self setNeedsUpdateConstraints];
	}
	return self;
}

- (UIView *)descriptionView {
	if (! _descriptionView) {
		_descriptionView = [UIView new];
		_descriptionView.translatesAutoresizingMaskIntoConstraints = NO;
        _descriptionView.backgroundColor = [self colorForSelectedView];
		_descriptionView.userInteractionEnabled = YES;
		UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDescriptionTap:)];
		[_descriptionView addGestureRecognizer:tapGesture];
		_descriptionView.tag = 1;
	}
	return _descriptionView;
}

- (UIView *)messageView {
	if (! _messageView) {
		_messageView = [UIView new];
		_messageView.translatesAutoresizingMaskIntoConstraints = NO;
        _messageView.backgroundColor = [self colorForUnselectedView];
		_messageView.userInteractionEnabled = YES;
		UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMessageTap:)];
		[_messageView addGestureRecognizer:tapGesture];
		_messageView.tag = 0;
	}
	return _messageView;
}

- (UILabel *)descriptionLabel {
	if (! _descriptionLabel) {
		_descriptionLabel = [UILabel new];
		_descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_descriptionLabel.text = @"Détails";
        _descriptionLabel.textColor = [self colorForSelectedText];
	}
	return _descriptionLabel;
}

- (UILabel *)messageLabel {
	if (! _messageLabel) {
		_messageLabel = [UILabel new];
		_messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_messageLabel.text = @"Messagerie";
        _messageLabel.textColor = [self colorForUnselectedText];
	}
	return _messageLabel;
}

- (void)setConstraintForDetailView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionView
													 attribute:NSLayoutAttributeCenterY
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterY
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionView
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeLeft
													multiplier:1.0 constant:0.0]];
}

- (void)setConstraintForMessageView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_messageView
													 attribute:NSLayoutAttributeCenterY
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterY
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_messageView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:1.0 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_messageView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5 constant:0.0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_messageView
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:_descriptionView
													 attribute:NSLayoutAttributeRight
													multiplier:1.0 constant:0.0]];
}

- (void)setConstraintForDescriptionLabel {
	[self.descriptionView addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionLabel
																attribute:NSLayoutAttributeCenterX
																relatedBy:NSLayoutRelationEqual
																   toItem:self.descriptionView
																attribute:NSLayoutAttributeCenterX
																multiplier:1.0 constant:0]];
	[self.descriptionView addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionLabel
																attribute:NSLayoutAttributeCenterY
																relatedBy:NSLayoutRelationEqual
																   toItem:self.descriptionView
																attribute:NSLayoutAttributeCenterY
																multiplier:1.0 constant:0]];
}
- (void)setConstraintForMessageLabel {
	[self.messageView addConstraint:[NSLayoutConstraint constraintWithItem:_messageLabel
																attribute:NSLayoutAttributeCenterX
																relatedBy:NSLayoutRelationEqual
																   toItem:self.messageView
																attribute:NSLayoutAttributeCenterX
																multiplier:1.0 constant:0]];
	[self.messageView addConstraint:[NSLayoutConstraint constraintWithItem:_messageLabel
																attribute:NSLayoutAttributeCenterY
																relatedBy:NSLayoutRelationEqual
																   toItem:self.messageView
																attribute:NSLayoutAttributeCenterY
																multiplier:1.0 constant:0]];
}

- (void)updateConstraints {
	if (! _didUpdateConstraints) {
		_didUpdateConstraints = YES;
		[self setConstraintForDetailView];
		[self setConstraintForMessageView];
		[self setConstraintForMessageLabel];
		[self setConstraintForDescriptionLabel];
	}
    [super updateConstraints];
}

#pragma mark - toolbox
- (UIColor *)colorForUnselectedText {
    return [UIColor colorWithRed:80/255. green:30/255. blue:150/255. alpha:1];
}

- (UIColor *)colorForSelectedText {
    return [UIColor whiteColor];
}

- (UIColor *)colorForSelectedView {
    return [UIColor colorWithWhite:1 alpha:0.35];
}

- (UIColor *)colorForUnselectedView {
    return [[UIColor blackColor] colorWithAlphaComponent:0.3];
}

#pragma mark - menutap
- (void)handleDescriptionTap:(UIGestureRecognizer *)gesture {
	if (_descriptionView.tag == 0) {
		_descriptionView.tag = 1;
		_messageView.tag = 0;
		_descriptionView.backgroundColor = [self colorForSelectedView];
		_descriptionLabel.textColor = [self colorForSelectedText];
		_messageView.backgroundColor = [self colorForUnselectedView];
		_messageLabel.textColor = [self colorForUnselectedText];
		[self noticeDelegateSelectedDescription];
	}
}

- (void)handleMessageTap:(UITapGestureRecognizer *)gesture {
	if (_messageView.tag == 0) {
		_messageView.tag = 1;
		_descriptionView.tag = 0;
		_messageView.backgroundColor = [self colorForSelectedView];
		_messageLabel.textColor = [self colorForSelectedText];
		_descriptionView.backgroundColor = [self colorForUnselectedView];
		_descriptionLabel.textColor = [self colorForUnselectedText];
		[self noticeDelegateSelectedMessage];
	}
}

#pragma mark - delegate notification

- (void)noticeDelegateSelectedMessage {
	if ([_delegate respondsToSelector:@selector(didMessageSelected)]) {
		[_delegate didMessageSelected];
	}
}

- (void)noticeDelegateSelectedDescription {
	if ([_delegate respondsToSelector:@selector(didDescriptionSelected)]) {
		[_delegate didDescriptionSelected];
	}
}

@end
