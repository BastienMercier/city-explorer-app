//
//  CEMasterEventDetailView.h
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CETopEventDetailView.h"
#import "CEDetailEventMenu.h"
#import "CEBotDetailEventView.h"
#import "CEEnums.h"
#import "CEBotDetailChat.h"

@interface CEMasterEventDetailView : UIView

@property (strong, nonatomic) CETopEventDetailView *topView;
@property (strong, nonatomic) CEDetailEventMenu *menu;
@property (strong, nonatomic) CEBotDetailEventView *botDetail;
@property (strong, nonatomic) CEBotDetailChat *tchat;

- (void)slideDescriptionView:(kCEDirection)direction;
@property (strong, nonatomic) NSLayoutConstraint *XBotAsis;

@end
