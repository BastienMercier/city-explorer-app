//
//  CEApiManager.m
//  ClienApi
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 Administrateur. All rights reserved.
//

#import "CEApiManager.h"
#import "PLSApi.h"
#import "CESpecificRequest.h"
#import "CERequest.h"

@implementation CEApiManager

- (void) executeParallel:(void (^) (void) )block{
	__block NSInteger count = 0;
	for (int i = 0; i < _requests.count; i++) {
		CERequest *r = _requests[i];
		[[PLSApi api] requestWithRequest:r callback:^(NSData *data, NSError *error) {
			api_request_handler_t handler = r.block;
			handler(data,error);
			count++;
			if (count == _requests.count) {
				block();
			}
		}];
	}
}


- (void) executeSeries;
{
	[self executeSeries:0];
}

- (void) executeSeries:(NSInteger)numberOfRequests;{
	__block NSInteger count = numberOfRequests;
	CERequest *r = _requests[count];
	[[PLSApi api] requestWithRequest:r callback:^(NSData *data, NSError *error) {
		api_request_handler_t handler = r.block;
		handler(data,error);
		count++;
		if (count == _requests.count) {
			return;
		} else {
			[self executeSeries:count];
		}
	}];
}

@end
