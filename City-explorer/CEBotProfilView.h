//
//  CEBotProfilView.h
//  City-explorer
//
//  Created by theo damaville on 15/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEBotProfilView : UIView

@property (strong, nonatomic) UIImageView *bgImage;
@property (strong, nonatomic) UITableView *tableEvents;

@end
