//
//  CEMasterEventDetailView.m
//  City-explorer
//
//  Created by Administrateur on 17/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEMasterEventDetailView.h"

@interface  CEMasterEventDetailView(){
	BOOL _didUpdateConstraints;
	NSLayoutConstraint *topViewConstraintY;
}
@end

@implementation CEMasterEventDetailView

- (instancetype)init {
	return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"detail_bg"]];
		[self addSubview:self.topView];
		[self addSubview:self.menu];
		[self addSubview:self.botDetail];
		[self addSubview:self.tchat];
		[self setNeedsUpdateConstraints];
	}
	return self;
}

- (CETopEventDetailView *)topView{
	if (! _topView) {
		_topView = [CETopEventDetailView new];
		_topView.translatesAutoresizingMaskIntoConstraints = NO;
	}
	return _topView;
}

- (CEDetailEventMenu *)menu {
	if (! _menu) {
		_menu = [CEDetailEventMenu new];
		_menu.translatesAutoresizingMaskIntoConstraints = NO;
	}
	return _menu;
}

- (CEBotDetailEventView *)botDetail {
	if (! _botDetail) {
		_botDetail = [CEBotDetailEventView new];
		_botDetail.translatesAutoresizingMaskIntoConstraints = NO;
	}
	return _botDetail;
}

- (CEBotDetailChat *)tchat {
	if (! _tchat) {
		_tchat = [CEBotDetailChat new];
		_tchat.translatesAutoresizingMaskIntoConstraints = NO;
	}
	return _tchat;
}

- (void)setConstraintForTopView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
													 attribute:NSLayoutAttributeCenterX
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterX
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_topView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.2 constant:0.0]];
	topViewConstraintY = [NSLayoutConstraint constraintWithItem:_topView
								 attribute:NSLayoutAttributeTop
								 relatedBy:NSLayoutRelationEqual
									toItem:self
								 attribute:NSLayoutAttributeTop
								multiplier:1.0 constant:65];
	[self addConstraint:topViewConstraintY];
}

- (void)setConstraintForMenuView {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_menu
													 attribute:NSLayoutAttributeCenterX
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeCenterX
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_menu
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_menu
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.09 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_menu
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:_topView
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
}

- (void)setConstraintForBotDetailView {
	_XBotAsis = [NSLayoutConstraint constraintWithItem:_botDetail
											 attribute:NSLayoutAttributeCenterX
											 relatedBy:NSLayoutRelationEqual
												toItem:self
											 attribute:NSLayoutAttributeCenterX
											multiplier:1.0 constant:0];
	[self addConstraint:_XBotAsis];
	
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_botDetail
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_botDetail
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:_menu
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_botDetail
													 attribute:NSLayoutAttributeBottom
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
}

- (void)setConstraintsForTchat {
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchat
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:self.menu
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchat
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self.botDetail
													 attribute:NSLayoutAttributeRight
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchat
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:1.0 constant:0]];
	[self addConstraint:[NSLayoutConstraint constraintWithItem:_tchat
													 attribute:NSLayoutAttributeBottom
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0 constant:0]];
}


- (void)updateConstraints {
	if (! _didUpdateConstraints) {
		_didUpdateConstraints = YES;
		[self setConstraintForTopView];
		[self setConstraintForMenuView];
		[self setConstraintForBotDetailView];
		[self setConstraintsForTchat];
	}
	[super updateConstraints];
}

#pragma mark animations
- (void)slideDescriptionView:(kCEDirection)direction {
	if (direction == kCEDirectionLeft) {
    	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    		_XBotAsis.constant = _XBotAsis.constant - self.frame.size.width;
			[self layoutIfNeeded];
    	} completion:nil];
	}
	else if (direction == kCEDirectionRight) {
    	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    		_XBotAsis.constant = _XBotAsis.constant + self.frame.size.width;
			[self layoutIfNeeded];
    	} completion:nil];
	}
}
@end
