//
//  CEUserRequest.m
//  City-explorer
//
//  Created by theo damaville on 28/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import "CEUserRequest.h"
#import "CECurrentUser.h"
#import "SDWebImageManager.h"

@implementation CEUserRequest

+ (CERequest *)getAllUsersWithBlock:(api_request_handler_t)block;
{
	CERequest *request = [CERequest new];
	
	request.endPoint = [NSString stringWithFormat:@"users"];
	request.method = @"GET";
	request.block = block;
	
	return request;
}

+ (CERequest *)getUserWithId:(NSString *)uid withBlock:(api_request_handler_t)block;
{
	CERequest *request = [CERequest new];
	
	request.endPoint = [NSString stringWithFormat:@"users/%@",uid];
	request.method = @"GET";
	request.block = block;
	
	return request;
}

+ (CERequest *)postCreateUser:(NSDictionary *)userInfos withBlock:(api_request_handler_t)block;
{
	CERequest *request = [CERequest new];
	
	request.endPoint = [NSString stringWithFormat:@"users"];
	request.method = @"POST";
	request.body = userInfos;
	request.block = block;
	
	return request;
}

+ (CERequest *)postToggleFollowUserWithId:(NSString *)uid withBlock:(api_request_handler_t)block;
{
	CERequest *request = [CERequest new];
	
	request.endPoint = [NSString stringWithFormat:@"users/toggleFollowing/%@",uid];
	request.method = @"POST";
	request.body = @{@"following":uid};
	request.block = block;
	
	return request;
}

+ (void) imageForUser:(NSString *)strURL withBlock:(void (^) (UIImage *userImage))block {
	SDWebImageManager *manager = [SDWebImageManager sharedManager];
	[manager downloadImageWithURL:[NSURL URLWithString:strURL] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
		if (image) {
			block(image);
		}
	}];
}

@end
