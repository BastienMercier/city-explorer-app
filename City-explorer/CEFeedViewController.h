//
//  CEFeedViewController.h
//  City-explorer
//
//  Created by Administrateur on 05/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEFeedView.h"
#import "CEUpdatedDataSource.h"
#import "CEFeedDataSource.h"

@interface CEFeedViewController : UIViewController<UITableViewDelegate, CEUpdatedDataSource, UITextFieldDelegate>

@property (strong, nonatomic) CEFeedView *contentView;
@property (strong, nonatomic) CEFeedDataSource *feedDataSource;

@end
