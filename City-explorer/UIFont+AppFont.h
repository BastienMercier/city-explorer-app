//
//  UIFOnt.h
//  City-explorer
//
//  Created by theo damaville on 01/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIFont (AppFont)

+ (UIFont *)appFontOfSize:(NSInteger)size;
+ (UIFont *)appFontLightOfSize:(NSInteger)size;

@end
