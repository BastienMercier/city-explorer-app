//
//  PLSAsyncRequest.m
//  AsyncConnexion
//
//  Created by Administrateur on 10/10/13.
//  Copyright (c) 2013 Administrateur. All rights reserved.
//

#import "PLSAsyncRequest.h"

@interface PLSAsyncRequest()
{
	NSMutableData* _data;
	api_request_handler_t _callback;;
}
@end

@implementation PLSAsyncRequest

+ (PLSAsyncRequest*) requestWithBlock:(api_request_handler_t)block;
{
	PLSAsyncRequest* obj = [PLSAsyncRequest alloc];
	if( ![obj initWithBlock:block])
	{
		return nil;
	}
	return obj;
}

- (PLSAsyncRequest*) initWithBlock:(api_request_handler_t)block
{
	if( !(self = [super init]))
	{
		return nil;
	}
	_callback = block;
	_data = [[NSMutableData alloc] init];
	return self;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
{
    _callback(nil,error);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
{
	[_data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
{
		_callback(_data,nil);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[_data setLength:0];
}
@end
