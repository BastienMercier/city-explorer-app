//
//  CECacheManager.h
//  City-explorer
//
//  Created by Administrateur on 03/12/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CECacheManager : NSObject
extern NSString *const kCECurrentUser;

- (void)saveCacheDataToDisk;
- (void)loadDiskDataIntoMemory;

- (void)setObject:(id)object forKey:(NSString *)aKey;
- (id)objectForKey:(NSString *)aKey;

@end
