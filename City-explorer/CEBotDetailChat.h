//
//  CEBotDetailChat.h
//  City-explorer
//
//  Created by Administrateur on 21/11/2014.
//  Copyright (c) 2014 BeerInc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEBotDetailChat : UIView

@property (strong, nonatomic) NSArray *participants;
@property (strong, nonatomic) UIScrollView *scrollingParticipants;
@property (strong, nonatomic) NSDictionary *tchatViews;
@property (strong, nonatomic) UITableView *tchatView;
@property (strong, nonatomic) UIView *greyView;
@property (strong, nonatomic) UITextView *message;
@property (strong, nonatomic) UIButton *sendButton;

@property (strong,nonatomic) NSLayoutConstraint *bottomConstraintForTableTchat;
@property (strong,nonatomic) NSLayoutConstraint *grayViewConstraintY;
- (void)moveTchatTextUp:(CGFloat)offset;
- (void)moveTchatTextDown:(CGFloat)offset;

@end
